#pragma once

#include <logging/logger.hpp>

#include <ostream>

namespace jaf::logging
{
	struct base
		: logger
	{
		base(level l, std::ostream& out);

		std::ostream& fatal() final;
		std::ostream& error() final;
		std::ostream& info() final;
		std::ostream& debug() final;
		std::ostream& trace() final;
	
	protected:
		virtual std::ostream& init(std::ostream& os);

	private:
		std::ostream& log(level l);

		level l_;
		std::ostream& out_;
	};
}

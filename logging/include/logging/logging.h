#pragma once

#define JAF_LOGGING_EXPORT __attribute__ ((visibility("default")))

#ifdef __cplusplus
extern "C" {
#endif

JAF_LOGGING_EXPORT void jaf_logging_setup();
JAF_LOGGING_EXPORT void jaf_logging_teardown();

#ifdef __cplusplus
}
#endif

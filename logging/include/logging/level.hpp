#pragma once

#include <ostream>
#include <istream>

namespace jaf::logging
{
	enum class level
	{
		none,
		fatal,
		error,
		info,
		debug,
		trace
	};

	std::ostream& operator<<(std::ostream& o, level l);
	std::istream& operator>>(std::istream& i, level& l);
}

#pragma once

#include <ostream>

namespace jaf::logging::details
{
	struct null_buffer
		: std::streambuf
	{
		int overflow(int c);
	};

	std::ostream& null();
}
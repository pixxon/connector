#pragma once

#include <logging/level.hpp>
#include <logging/logger.hpp>

namespace jaf::logging
{
	namespace details
	{
		std::ostream& get_stream();

		template<class T>
		logger& get_logger()
		{
			static auto logger_ = T{ get_stream() };
			return logger_;
		}
	}

	void setup(std::ostream& s);
	void teardown();

	template<class... Args>
	std::ostream& log(std::ostream& l, Args... args)
	{
		return (l << ... << args) << std::endl;
	}
}

#define LOG_INTERNAL(c, l, ...) ::jaf::logging::log(::jaf::logging::details::get_logger<c>().l(), "[", __PRETTY_FUNCTION__ , "] " __VA_OPT__(,) __VA_ARGS__)

#if RELEASE
	#define TRACE(c, ...)
	#define DEBUG(c, ...)
	#define INFO(c, ...) LOG_INTERNAL(c, info, __VA_ARGS__)
	#define ERROR(c, ...) LOG_INTERNAL(c, error, __VA_ARGS__)
	#define FATAL(c, ...) LOG_INTERNAL(c, fatal, __VA_ARGS__)
#else
	#define TRACE(c, ...) LOG_INTERNAL(c, trace, __VA_ARGS__)
	#define DEBUG(c, ...) LOG_INTERNAL(c, debug, __VA_ARGS__)
	#define INFO(c, ...) LOG_INTERNAL(c, info, __VA_ARGS__)
	#define ERROR(c, ...) LOG_INTERNAL(c, error, __VA_ARGS__)
	#define FATAL(c, ...) LOG_INTERNAL(c, fatal, __VA_ARGS__)
#endif

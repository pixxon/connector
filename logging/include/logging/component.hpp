#pragma once

#include <logging/base.hpp>

#include <sstream>

namespace jaf::logging
{
	template<class T>
	struct component
		: base
	{
		component(std::ostream& out)
			: base{ get_level(), out }
		{
		}

		std::ostream& init(std::ostream& os)
		{
			return (base::init(os) << "[" << T::component_name << "]");
		}

		static level get_level()
		{
			const auto env_name = std::string{"jaf_"} + T::component_name + std::string{"_logging"};
			if(const auto* ls = std::getenv(env_name.c_str()); ls != nullptr)
			{
				auto ss = std::stringstream{ ls };
				auto l = level::info;
				ss >> l;
				return l;
			}
			else
			{
				return level::trace;
			}
		}
	};
}

#include <logging/logger.hpp>

#include <stdexcept>

namespace jaf::logging
{
	logger::~logger() = default;
}

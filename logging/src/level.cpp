#include <logging/level.hpp>

#include <stdexcept>

namespace jaf::logging
{
	std::ostream& operator<<(std::ostream& o, level l)
	{
		switch(l)
		{
			case level::none:
				o << "none";
				break;
			case level::fatal:
				o << "fatal";
				break;
			case level::error:
				o << "error";
				break;
			case level::info:
				o << "info";
				break;
			case level::debug:
				o << "debug";
				break;
			case level::trace:
				o << "trace";
				break;
		}

		return o;
	}
	
	std::istream& operator>>(std::istream& i, level& l)
	{
		if (auto s = std::string{}; i >> s)
		{
			if ("none" == s)
			{
				l = level::none;
			}
			else if ("fatal" == s)
			{
				l = level::fatal;
			}
			else if ("error" == s)
			{
				l = level::error;
			}
			else if ("info" == s)
			{
				l = level::info;
			}
			else if ("debug" == s)
			{
				l = level::debug;
			}
			else if ("trace" == s)
			{
				l = level::trace;
			}
			else
			{
				throw std::invalid_argument{s + " is not a valid value for logging level!"};
			}
		}
		else
		{
			throw std::invalid_argument{"Could not read value for logging level!"};
		}
		
		return i;
	}
}

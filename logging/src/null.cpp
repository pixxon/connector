#include <logging/null.hpp>

namespace jaf::logging::details
{
	std::ostream& null()
	{
		static null_buffer buff;
		static std::ostream null{ &buff };
		return null;
	}

	int null_buffer::overflow(int c)
	{
		return c;
	}
}

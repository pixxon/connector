#include <logging/logging.h>
#include <logging/logging.hpp>
#include <logging/null.hpp>

#include <iostream>

namespace jaf::logging
{
	namespace details
	{
		struct stream_holder
		{
			std::ostream* stream = std::addressof(details::null());
//			std::ostream* stream = std::addressof(std::cout);
		};

		stream_holder& singleton()
		{
			static auto s = stream_holder{};
			return s;
		}

		std::ostream& get_stream()
		{
			return *(singleton().stream);
		}
	}

	void setup(std::ostream& s)
	{
		details::singleton().stream = std::addressof(s);
	}

	void teardown()
	{
		details::singleton().stream = nullptr;
	}
}

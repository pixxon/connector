import os
import re
import subprocess
import sys
from pathlib import Path

from setuptools import Extension, setup
from setuptools.command.build_ext import build_ext

PLAT_TO_CMAKE = {
    "win32": "Win32",
    "win-amd64": "x64",
    "win-arm32": "ARM",
    "win-arm64": "ARM64",
}

class CMakeExtension(Extension):
    def __init__(self, name, sources, sourcedir) -> None:
        super().__init__(name=name, sources=sources)
        self.sourcedir = os.fspath(Path(sourcedir).resolve())

class CMakeBuild(build_ext):
    def build_extension(self, ext) -> None:
        ext_fullpath = Path.cwd() / self.get_ext_fullpath(ext.name)
        extdir = ext_fullpath.parent.resolve()

        debug = int(os.environ.get("DEBUG", 0)) if self.debug is None else self.debug
        cfg = "Debug" if debug else "Release"

        cmake_generator = os.environ.get("CMAKE_GENERATOR", "")

        cmake_provider_path = Path(ext.sourcedir) / "cmake" / "conan_provider.cmake"
        cmake_args = [
            f"-DCMAKE_INSTALL_PREFIX={extdir}{os.sep}{ext.name}{os.sep}",
            f"-DPYTHON_EXECUTABLE={sys.executable}",
            f"-DCMAKE_BUILD_TYPE={cfg}",
            f"-DCMAKE_PROJECT_TOP_LEVEL_INCLUDES={cmake_provider_path}",
        ]
        build_args = ["--target", "install"]

        if "CMAKE_ARGS" in os.environ:
            cmake_args += [item for item in os.environ["CMAKE_ARGS"].split(" ") if item]

        cmake_args += [f"-DEXTENSION_TEMPLATE_VERSION={self.distribution.get_version()}"]

        if self.compiler.compiler_type != "msvc":
            if not cmake_generator or cmake_generator == "Ninja":
                try:
                    import ninja # type: ignore

                    ninja_executable_path = Path(ninja.BIN_DIR) / "ninja"
                    cmake_args += [
                        "-GNinja",
                        f"-DCMAKE_MAKE_PROGRAM:FILEPATH={ninja_executable_path}",
                    ]
                except ImportError:
                    pass

        else:
            single_config = any(x in cmake_generator for x in {"NMake", "Ninja"})
            contains_arch = any(x in cmake_generator for x in {"ARM", "Win64"})
            if not single_config and not contains_arch:
                cmake_args += ["-A", PLAT_TO_CMAKE[self.plat_name]]
            if not single_config:
                build_args += ["--config", cfg]

        if sys.platform.startswith("darwin"):
            archs = re.findall(r"-arch (\S+)", os.environ.get("ARCHFLAGS", ""))
            if archs:
                cmake_args += ["-DCMAKE_OSX_ARCHITECTURES={}".format(";".join(archs))]

        if "CMAKE_BUILD_PARALLEL_LEVEL" not in os.environ:
            if hasattr(self, "parallel") and self.parallel:
                build_args += [f"-j{self.parallel}"]

        build_temp = Path(self.build_temp) / ext.name
        if not build_temp.exists():
            build_temp.mkdir(parents=True)

        subprocess.run(
            ["cmake", ext.sourcedir, *cmake_args], cwd=build_temp, check=True
        )
        subprocess.run(
            ["cmake", "--build", ".", *build_args], cwd=build_temp, check=True
        )

setup_args = dict(
    ext_modules = [
        CMakeExtension(
            'connector_binding',
            [
                'src/connector_binding/CMakeLists.txt',
                'src/connector_binding/conanfile.txt',
                'src/connector_binding/cmake/conan_provider.cmake',
                'src/connector_binding/src/details/value_converter.hpp',
                'src/connector_binding/src/details/value.hpp',
                'src/connector_binding/src/connector_binding_pch.hpp',
                'src/connector_binding/src/core.hpp',
                'src/connector_binding/src/external_legacy.hpp',
                'src/connector_binding/src/external_object.hpp',
                'src/connector_binding/src/legacy.hpp',
                'src/connector_binding/src/object.hpp',
                'src/connector_binding/src/root_context.hpp',
                'src/connector_binding/src/value.hpp',
                'src/connector_binding/src/details/object.cpp',
                'src/connector_binding/src/details/value_converter.cpp',
                'src/connector_binding/src/details/value.cpp',
                'src/connector_binding/src/core.cpp',
                'src/connector_binding/src/external_legacy.cpp',
                'src/connector_binding/src/external_object.cpp',
                'src/connector_binding/src/legacy.cpp',
                'src/connector_binding/src/object.cpp',
                'src/connector_binding/src/root_context.cpp',
                'src/connector_binding/src/value.cpp'
            ],
            'src/connector_binding'
        )
    ],
    cmdclass={
        "build_ext": CMakeBuild
    }
)
setup(**setup_args)

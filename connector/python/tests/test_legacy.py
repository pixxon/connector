import connector

import unittest

class TestLegacy(unittest.TestCase):

    def test_call1(self):
        l = connector.Legacy()
        l.call1()
    

    def test_call2(self):
        class helper:
            def __init__(self):
                self.count = 0
            
            def __call__(self):
                self.count = self.count + 1

        callback = helper()

        l = connector.Legacy()
        l.call2(callback)

        self.assertEqual(1, callback.count)

    def test_call3(self):
        class helper:
            def __init__(self):
                self.count = 0
            
            def __call__(self, args):
                self.count = self.count + 1
                return sum(args)
    
        callback = helper()

        l = connector.Legacy()
        result = l.call3(callback)

        self.assertEqual(1, callback.count)
        self.assertEqual(14, result)
        
import connector

import unittest

class TestImpl(connector.ExternalObjectImpl):
    def callMethod(self, id, args):
        result = id + args[0].getInt32() + args[1].getInt32()
        return connector.Value(result)

class TestExternalObject(unittest.TestCase):

    def test_callMethod(self):
        obj = connector.ExternalObject(TestImpl())
        result = obj.callMethod(0, [connector.Value(1), connector.Value(2)])
        self.assertEqual(connector.Value(3), result)

import connector

import unittest

class TestImpl(connector.ExternalObjectImpl):
    def callMethod(self, id, args):
        result = id + args[0].getInt32() + args[1].getInt32()
        return connector.Value(result)

class TestRootContext(unittest.TestCase):

    def test_no_object(self):
        obj = connector.RootContext.get("calculator")

        self.assertEqual(obj.handle(), 0)

    def test_has_object(self):
        connector.RootContext.registerObject("calculator", connector.ExternalObject(TestImpl()))

        obj = connector.RootContext.get("calculator")
        result = obj.callMethod(0, [connector.Value(1), connector.Value(2)])

        self.assertEqual(connector.Value(3), result)

        connector.RootContext.removeObject("calculator")

        obj = connector.RootContext.get("calculator")
        self.assertEqual(obj.handle(), 0)

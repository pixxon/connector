import connector

import unittest

class TestExternalLegacy(unittest.TestCase):

    def test_call1(self):
        l = connector.ExternalLegacy(connector.ExternalLegacyImpl())
        l.call1()
    

    def test_call2(self):
        class helper:
            def __init__(self):
                self.count = 0
            
            def __call__(self):
                self.count = self.count + 1

        callback = helper()

        l = connector.ExternalLegacy(connector.ExternalLegacyImpl())
        l.call2(callback)

        self.assertEqual(1, callback.count)

    def test_call3(self):
        class helper:
            def __init__(self):
                self.count = 0
            
            def __call__(self, args):
                self.count = self.count + 1
                return sum(args)
    
        callback = helper()

        l = connector.ExternalLegacy(connector.ExternalLegacyImpl())
        result = l.call3(callback)

        self.assertEqual(1, callback.count)
        self.assertEqual(14, result)
        
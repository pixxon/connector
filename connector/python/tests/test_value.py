from connector import Value, ExternalObject

import unittest

class TestValue(unittest.TestCase):
    def test_int32(self):
        v = Value(5)
        self.assertTrue(v.isInt32())
        self.assertEqual(5, v.getInt32())
    
    def test_bool(self):
        v = Value(True)
        self.assertTrue(v.isBool())
        self.assertTrue(v.getBool())

    def test_object(self):
        obj = ExternalObject(None)
        v = Value(obj)
        self.assertTrue(v.isObject())
        self.assertEqual(obj, v.getObject())

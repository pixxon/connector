import connector_binding

class Object:
	def __init__(self, handle, addRef = True):
		self.__handle = handle
		if addRef:
			connector_binding.object.ref__(self.__handle)

	def __del__(self):
		connector_binding.object.unref__(self.__handle)

	def callMethod(self, id, args):
		return connector_binding.object.callMethod(self.__handle, id, args)
	
	def handle(self):
		return self.__handle

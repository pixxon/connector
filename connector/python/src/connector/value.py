from enum import Enum

import connector_binding

from .object import Object

class ValueType(Enum):
	OBJECT = 0,
	INT32 = 1,
	BOOL = 2

class Value:
	def __init__(self, obj):
		self.__obj = obj
		if (isinstance(obj, bool)): # order matters!
			self.__type = ValueType.BOOL
		elif (isinstance(obj, int)):
			self.__type = ValueType.INT32
		elif (isinstance(obj, Object)):
			self.__type = ValueType.OBJECT
		else:
			raise "invalid type"

	def __eq__(self, other):
		return self.__type == other.__type and self.__obj == other.__obj

	def isBool(self):
		return self.__type == ValueType.BOOL;

	def getBool(self):
		return self.__obj

	def isObject(self):
		return self.__type == ValueType.OBJECT;

	def getObject(self):
		return self.__obj

	def isInt32(self):
		return self.__type == ValueType.INT32;

	def getInt32(self):
		return self.__obj

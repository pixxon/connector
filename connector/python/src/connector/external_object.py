import connector_binding

from .object import Object

class ExternalObjectImpl:
	def callMethod(self, id, args):
		pass

class ExternalObject(Object):
	def __init__(self, impl):
		super(ExternalObject, self).__init__(connector_binding.external_object.create__(impl), False)
		self.__impl = impl

	def callMethod(self, id, args):
		return self.__impl.callMethod(id, args)

import connector_binding

class Legacy:
	def __init__(self, handle = None, addRef = True):
		self.__handle = handle
		if (self.__handle is None):
			self.__handle = connector_binding.legacy.create__()
		elif addRef:
			connector_binding.legacy.ref__(self.__handle)

	def __del__(self):
		connector_binding.legacy.unref__(self.__handle)

	def call1(self):
		return connector_binding.legacy.call1(self.__handle)

	def call2(self, callable):
		return connector_binding.legacy.call2(self.__handle, callable)

	def call3(self, callable):
		return connector_binding.legacy.call3(self.__handle, callable)

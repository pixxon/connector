from importlib.util import spec_from_file_location, module_from_spec
from pathlib import Path
from sys import modules

def alternative_load_dynamic(name, path, file=None):
	spec = spec_from_file_location(name, path)
	module = module_from_spec(spec)
	modules[name] = module
	spec.loader.exec_module(module)
	return modules[name]

alternative_load_dynamic('connector_binding', Path(__file__).parent.parent / 'connector_binding' / 'libconnector_binding.so')

from .legacy import Legacy
from .external_legacy import ExternalLegacy, ExternalLegacyImpl
from .object import Object
from .external_object import ExternalObject,ExternalObjectImpl
from .root_context import RootContext
from .value import Value

import connector_binding

from .legacy import Legacy

class ExternalLegacyImpl:
	def call1(self):
		pass

	def call2(self, callable):
		callable()

	def call3(self, callable):
		return callable([3, 4]) * 2

class ExternalLegacy(Legacy):
	def __init__(self, impl):
		super(ExternalLegacy, self).__init__(connector_binding.external_legacy.create__(impl), False)
		self.__impl = impl

	def call1(self):
		return self.__impl.call1()

	def call2(self, callable):
		return self.__impl.call2(callable)

	def call3(self, callable):
		return self.__impl.call3(callable)

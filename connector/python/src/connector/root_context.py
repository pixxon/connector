import connector_binding

from .object import Object

class RootContext:
	@staticmethod
	def get(path):
		return Object(connector_binding.root_context.get(path), False)
	
	@staticmethod
	def registerObject(path, obj):
		return connector_binding.root_context.registerObject(path, obj.handle())

	@staticmethod
	def removeObject(path):
		return connector_binding.root_context.removeObject(path)

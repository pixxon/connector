#pragma once

#include "connector_binding_pch.hpp"

#include <connector/external_legacy.h>

namespace connector::binding {
struct external_legacy {
    struct call2_callback {
        PyObject_HEAD
            connector_legacy_call2_callback_type callback;

        static PyObject* call(PyObject* self, PyObject* args, PyObject* kwds);

        static PyTypeObject type;
    };

    struct call3_callback {
        PyObject_HEAD
            connector_legacy_call3_callback_type callback;

        static PyObject* call(PyObject* self, PyObject* args, PyObject* kwds);

        static PyTypeObject type;
    };

    static PyObject* create__(PyObject* self, PyObject* args);

    static PyObject* module__();
};
}

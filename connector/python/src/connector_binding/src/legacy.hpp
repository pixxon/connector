#pragma once

#include "connector_binding_pch.hpp"

#include <connector/legacy.h>

namespace connector::binding {
struct legacy {
    static PyObject* create__(PyObject* self, PyObject* args);
    static PyObject* ref__(PyObject* self, PyObject* args);
    static PyObject* unref__(PyObject* self, PyObject* args);
    static PyObject* call1(PyObject* self, PyObject* args);
    static PyObject* call2(PyObject* self, PyObject* args);
    static PyObject* call3(PyObject* self, PyObject* args);

    static PyObject* module__();
};

}

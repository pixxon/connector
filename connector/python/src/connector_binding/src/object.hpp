#pragma once

#include "connector_binding_pch.hpp"

#include <connector/object.h>

namespace connector::binding {
struct object {
    static PyObject* ref__(PyObject* self, PyObject* args);
    static PyObject* unref__(PyObject* self, PyObject* args);
    static PyObject* callMethod(PyObject* self, PyObject* args);

    static PyObject* module__();
};

}

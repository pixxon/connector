#include "connector_binding_pch.hpp"

#include "core.hpp"

#include "external_legacy.hpp"
#include "external_object.hpp"
#include "legacy.hpp"
#include "object.hpp"
#include "root_context.hpp"

#include "details/value.hpp"

#ifdef __cplusplus
extern "C" {
#endif

PyMODINIT_FUNC PyInit_connector_binding()
{
    static PyMethodDef methods[] = {
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "connector_binding",
        nullptr,
        -1,
        methods
    };

    auto m = PyModule_Create(&module);
    PyModule_AddObject(m, "legacy", connector::binding::legacy::module__());
    PyModule_AddObject(m, "external_legacy", connector::binding::external_legacy::module__());
    PyModule_AddObject(m, "object", connector::binding::object::module__());
    PyModule_AddObject(m, "external_object", connector::binding::external_object::module__());
    PyModule_AddObject(m, "root_context", connector::binding::root_context::module__());

    return m;
}

#ifdef __cplusplus
}
#endif

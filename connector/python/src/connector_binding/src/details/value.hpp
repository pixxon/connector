#pragma once

#include "connector_binding_pch.hpp"

#include <connector/value.h>

namespace connector::binding::details {
struct value {
    explicit value(PyObject*);

    explicit value(PyObject*, int32_t);
    explicit value(PyObject*, bool);

    ~value();

    bool isBool() const;
    bool getBool() const;

    bool isInt32() const;
    int32_t getInt32() const;

    bool isObject() const;
    PyObject* getObject() const;

    PyObject* release();

private:
    PyObject* self_ = nullptr;
};

connector_value value_converter(const value&);
value value_converter(connector_value);

}

#pragma once

#include "connector_binding_pch.hpp"

#include <connector/value.h>

#include "value.hpp"

namespace connector::binding::details {
connector_value value_converter(const value&);
value value_converter(connector_value);
}

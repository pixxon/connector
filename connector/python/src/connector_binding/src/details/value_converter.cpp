#include "connector_binding_pch.hpp"

#include "value_converter.hpp"

namespace connector::binding::details {
connector_value value_converter(const value& v)
{
    if (v.isBool()) {
        return connector_value_createBool__(v.getBool());
    } else if (v.isInt32()) {
        return connector_value_createInt32__(v.getInt32());
    } else if (v.isObject()) {
        //			return connector_value_createObject__( reinterpret_cast<connector_binding_object_handle>( PyLong_AsVoidPtr( PyObject_CallMethod( PyObject_CallMethod(e, "getInt32", nullptr) ), "handle", nullptr ) ) );
        //			return connector_value_createObject__( reinterpret_cast<connector_binding_object_handle>( v.getObject().detach() ) );
    }
    throw std::runtime_error { "invalid value type!" };
}

value value_converter(connector_value v)
{
    PyObject* module_ = PyImport_ImportModule("connector");
    PyObject* class_ = PyObject_GetAttrString(module_, "Value");

    if (connector_value_isBool(v)) {
        return value { class_, connector_value_getBool(v) };
    } else if (connector_value_isInt32(v)) {
        return value { class_, connector_value_getInt32(v) };
    } else if (connector_value_isObject(v)) {
        //			return connector::core::value{ connector::core::make_intrusive<connector::core::object, false>( reinterpret_cast<connector::core::object*>( connector_value_getObject(v) ) ) };
        //			return value{ connector::core::make_intrusive<connector::core::object, false>( reinterpret_cast<connector::core::object*>( connector_value_getObject(v) ) ) };
    }
    throw std::runtime_error { "invalid value type!" };
}
}

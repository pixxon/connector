#include "connector_binding_pch.hpp"

#include "value.hpp"

namespace connector::binding::details {
value::value(PyObject* self)
    : self_ { self }
{
}

value::value(PyObject* class__, int32_t i)
    : self_ { nullptr }
{
    PyObject* param = PyLong_FromLong(i);
    PyObject* params = PyTuple_Pack(1, param);

    self_ = PyObject_CallObject(class__, params);

    Py_DECREF(params);
    Py_DECREF(param);
}

value::value(PyObject* class__, bool b)
    : self_ { PyObject_CallObject(class__, PyTuple_Pack(1, PyBool_FromLong(b))) }
{
}

value::~value()
{
    if (self_ != nullptr) {
        Py_DECREF(self_);
    }
}

bool value::isBool() const
{
    auto pyResult = PyObject_CallMethod(self_, "isBool", nullptr);
    auto result = PyObject_IsTrue(pyResult);
    Py_DECREF(pyResult);
    return result;
}

bool value::getBool() const
{
    auto pyResult = PyObject_CallMethod(self_, "getBool", nullptr);
    auto result = PyObject_IsTrue(pyResult);
    Py_DECREF(pyResult);
    return result;
}

bool value::isInt32() const
{
    auto pyResult = PyObject_CallMethod(self_, "getInt32", nullptr);
    auto result = PyObject_IsTrue(pyResult);
    Py_DECREF(pyResult);
    return result;
}

int32_t value::getInt32() const
{
    auto pyResult = PyObject_CallMethod(self_, "getInt32", nullptr);
    auto result = PyLong_AsLong(pyResult);
    Py_DECREF(pyResult);
    return result;
}

bool value::isObject() const
{
    auto pyResult = PyObject_CallMethod(self_, "isObject", nullptr);
    auto result = PyObject_IsTrue(pyResult);
    Py_DECREF(pyResult);
    return result;
}

PyObject* value::getObject() const
{
    auto pyResult = PyObject_CallMethod(self_, "getObject", nullptr);
    //		auto result = PyLong_AsLong( pyResult );
    //		Py_DECREF(pyResult);
    return pyResult;
}

PyObject* value::release()
{
    auto result = self_;
    self_ = nullptr;
    return result;
}
}

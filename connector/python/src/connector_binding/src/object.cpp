#include "connector_binding_pch.hpp"

#include "object.hpp"

#include "details/value.hpp"
#include "details/value_converter.hpp"

namespace connector::binding {
PyObject* object::ref__(PyObject* self, PyObject* args)
{
    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    connector_object_ref__(static_cast<connector_object_handle>(PyLong_AsVoidPtr(handle)));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* object::unref__(PyObject* self, PyObject* args)
{
    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    connector_object_unref__(static_cast<connector_object_handle>(PyLong_AsVoidPtr(handle)));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* object::callMethod(PyObject* self, PyObject* args)
{
    PyObject* handle;
    PyObject* id;
    PyObject* list;
    if (!PyArg_ParseTuple(args, "O|O|O", &handle, &id, &list)) {
        return nullptr;
    }
    size_t argc = PyList_Size(list);
    std::vector<connector_value> cArgs;
    for (auto i = 0u; i < argc; ++i) {
        PyObject* e = PyList_GetItem(list, i);
        Py_INCREF(e);
        cArgs.push_back(details::value_converter(details::value { e }));
    }

    auto result = connector_object_callMethod(static_cast<connector_object_handle>(PyLong_AsVoidPtr(handle)), PyLong_AsLong(id), cArgs.size(), cArgs.data());
    auto pyResult = details::value_converter(result);
    auto asd = pyResult.release();
    Py_INCREF(asd);
    return asd;
}

PyObject* object::module__()
{
    static PyMethodDef methods[] = {
        { "ref__", ref__, METH_VARARGS, nullptr },
        { "unref__", unref__, METH_VARARGS, nullptr },
        { "callMethod", callMethod, METH_VARARGS, nullptr },
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "object",
        nullptr,
        -1,
        methods
    };
    auto obj = PyModule_Create(&module);
    return obj;
}
}

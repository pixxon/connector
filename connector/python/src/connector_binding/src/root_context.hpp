#pragma once

#include "connector_binding_pch.hpp"

#include <connector/root_context.h>

namespace connector::binding {
struct root_context {
    static PyObject* get(PyObject* type, PyObject* args);
    static PyObject* registerObject(PyObject* type, PyObject* args);
    static PyObject* removeObject(PyObject* type, PyObject* args);

    static PyObject* module__();
};

}

#include "connector_binding_pch.hpp"

#include "root_context.hpp"

namespace connector::binding {
PyObject* root_context::get(PyObject* self, PyObject* args)
{
    PyObject* path;
    if (!PyArg_ParseTuple(args, "O", &path)) {
        return nullptr;
    }
    const auto* cPath = PyUnicode_AsUTF8(path);
    const auto cHandle = connector_root_context_get(cPath);
    return PyLong_FromVoidPtr(cHandle);
}

PyObject* root_context::registerObject(PyObject* self, PyObject* args)
{
    PyObject* path;
    PyObject* obj;
    if (!PyArg_ParseTuple(args, "O|O", &path, &obj)) {
        return nullptr;
    }
    connector_root_context_registerObject(PyUnicode_AsUTF8(path), reinterpret_cast<connector_object_handle>(PyLong_AsVoidPtr(obj)));

    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* root_context::removeObject(PyObject* self, PyObject* args)
{
    PyObject* path;
    if (!PyArg_ParseTuple(args, "O", &path)) {
        return nullptr;
    }
    connector_root_context_removeObject(PyUnicode_AsUTF8(path));

    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* root_context::module__()
{
    static PyMethodDef methods[] = {
        { "get", get, METH_VARARGS, nullptr },
        { "registerObject", registerObject, METH_VARARGS, nullptr },
        { "removeObject", removeObject, METH_VARARGS, nullptr },
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "root_context",
        nullptr,
        -1,
        methods
    };
    auto obj = PyModule_Create(&module);
    return obj;
}
}

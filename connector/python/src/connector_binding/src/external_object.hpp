#pragma once

#include "connector_binding_pch.hpp"

#include <connector/external_object.h>

namespace connector::binding {
struct external_object {
    static PyObject* create__(PyObject* self, PyObject* args);

    static PyObject* module__();
};
}

#include "connector_binding_pch.hpp"

#include "legacy.hpp"

namespace connector::binding {
PyObject* legacy::create__(PyObject* self, PyObject* args)
{
    return PyLong_FromVoidPtr(connector_legacy_create__());
}

PyObject* legacy::ref__(PyObject* self, PyObject* args)
{
    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    connector_legacy_ref__(static_cast<connector_legacy_handle>(PyLong_AsVoidPtr(handle)));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* legacy::unref__(PyObject* self, PyObject* args)
{
    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    connector_legacy_unref__(static_cast<connector_legacy_handle>(PyLong_AsVoidPtr(handle)));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* legacy::call1(PyObject* self, PyObject* args)
{
    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    connector_legacy_call1(static_cast<connector_legacy_handle>(PyLong_AsVoidPtr(handle)));
    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* legacy::call2(PyObject* self, PyObject* args)
{
    PyObject* handle;
    PyObject* callable;
    if (!PyArg_ParseTuple(args, "O|O", &handle, &callable)) {
        return nullptr;
    }
    Py_INCREF(callable);

    connector_legacy_call2_callback_type cb;
    cb.callback = [](void* userdata) {
        PyObject_Call(static_cast<PyObject*>(userdata), PyTuple_New(0), nullptr);
    };
    cb.userdata = static_cast<void*>(callable);
    cb.userdata_destructor = [](void* userdata) {
        Py_DECREF(static_cast<PyObject*>(userdata));
    };
    connector_legacy_call2(static_cast<connector_legacy_handle>(PyLong_AsVoidPtr(handle)), cb);

    Py_INCREF(Py_None);
    return Py_None;
}

PyObject* legacy::call3(PyObject* self, PyObject* args)
{
    PyObject* handle;
    PyObject* callable;
    if (!PyArg_ParseTuple(args, "O|O", &handle, &callable)) {
        return nullptr;
    }
    Py_INCREF(callable);

    connector_legacy_call3_callback_type cb;
    cb.callback = [](void* userdata, size_t argc, const int64_t* argv) {
        auto list = PyList_New(argc);
        for (size_t i = 0; i < argc; ++i) {
            PyList_SetItem(list, i, PyLong_FromLong(*(argv + i)));
        }
        return PyLong_AsLong(PyObject_Call(static_cast<PyObject*>(userdata), PyTuple_Pack(1, list), nullptr));
    };
    cb.userdata = static_cast<void*>(callable);
    cb.userdata_destructor = [](void* userdata) {
        Py_DECREF(static_cast<PyObject*>(userdata));
    };
    auto result = connector_legacy_call3(static_cast<connector_legacy_handle>(PyLong_AsVoidPtr(handle)), cb);
    return PyLong_FromLong(result);
}

PyObject* legacy::module__()
{
    static PyMethodDef methods[] = {
        { "create__", create__, METH_VARARGS, nullptr },
        { "ref__", ref__, METH_VARARGS, nullptr },
        { "unref__", unref__, METH_VARARGS, nullptr },
        { "call1", call1, METH_VARARGS, nullptr },
        { "call2", call2, METH_VARARGS, nullptr },
        { "call3", call3, METH_VARARGS, nullptr },
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "legacy",
        nullptr,
        -1,
        methods
    };
    auto obj = PyModule_Create(&module);
    return obj;
}
}

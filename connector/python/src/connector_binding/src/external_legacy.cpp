#include "connector_binding_pch.hpp"

#include "external_legacy.hpp"

namespace connector::binding {
PyTypeObject external_legacy::call2_callback::type = {
    PyObject_HEAD_INIT(NULL) "call2_callback",
    sizeof(call2_callback),
    0,
    nullptr,
    0,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    call2_callback::call,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    Py_TPFLAGS_DEFAULT,
    "call2_callback",
    nullptr,
    nullptr,
    nullptr,
    0,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    0,
    nullptr,
    nullptr,
    PyType_GenericNew
};

PyObject* external_legacy::call2_callback::call(PyObject* self, PyObject* args, PyObject* kwds)
{
    auto ptr = reinterpret_cast<external_legacy::call2_callback*>(self);
    ptr->callback.callback(ptr->callback.userdata);
    if (ptr->callback.userdata_destructor != nullptr) {
        ptr->callback.userdata_destructor(ptr->callback.userdata);
    }
    Py_INCREF(Py_None);
    return Py_None;
}

PyTypeObject external_legacy::call3_callback::type = {
    PyObject_HEAD_INIT(NULL) "call3_callback",
    sizeof(external_legacy::call3_callback),
    0,
    nullptr,
    0,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    external_legacy::call3_callback::call,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    Py_TPFLAGS_DEFAULT,
    "call3_callback",
    nullptr,
    nullptr,
    nullptr,
    0,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    nullptr,
    0,
    nullptr,
    nullptr,
    PyType_GenericNew
};

PyObject* external_legacy::call3_callback::call(PyObject* self, PyObject* args, PyObject* kwds)
{
    auto ptr = reinterpret_cast<external_legacy::call3_callback*>(self);

    PyObject* list;
    if (!PyArg_ParseTuple(args, "O", &list)) {
        return nullptr;
    }

    auto lsize = PyList_Size(list);
    std::vector<int64_t> asdasd;
    asdasd.reserve(lsize);
    for (auto i = 0u; i != lsize; ++i) {
        auto e = PyList_GetItem(list, i);
        asdasd.push_back(PyLong_AsLong(e));
    }

    auto res = ptr->callback.callback(ptr->callback.userdata, asdasd.size(), asdasd.data());
    if (ptr->callback.userdata_destructor != nullptr) {
        ptr->callback.userdata_destructor(ptr->callback.userdata);
    }
    return PyLong_FromLong(res);
}

struct external_legacy_implementation {
    static void destroy__(connector_external_legacy_handle self)
    {
        auto ptr = reinterpret_cast<PyObject*>(self);
        Py_DECREF(ptr);
    }

    static void call1(connector_external_legacy_handle self)
    {
        auto ptr = reinterpret_cast<PyObject*>(self);
        PyObject_CallMethod(ptr, "call1", nullptr);
    }

    static void call2(connector_external_legacy_handle self, connector_legacy_call2_callback_type callback)
    {
        auto cb = PyObject_New(external_legacy::call2_callback, &external_legacy::call2_callback::type);
        cb->callback = callback;

        auto ptr = reinterpret_cast<PyObject*>(self);
        PyObject_CallMethod(ptr, "call2", "O", reinterpret_cast<PyObject*>(cb));
        Py_DECREF(cb);
    }

    static int64_t call3(connector_external_legacy_handle self, connector_legacy_call3_callback_type callback)
    {
        auto cb = PyObject_New(external_legacy::call3_callback, &external_legacy::call3_callback::type);
        cb->callback = callback;

        auto ptr = reinterpret_cast<PyObject*>(self);
        auto res = PyObject_CallMethod(ptr, "call3", "O", reinterpret_cast<PyObject*>(cb));
        Py_DECREF(cb);

        return PyLong_AsLong(res);
    }
};

PyObject* external_legacy::create__(PyObject* self, PyObject* args)
{
    static connector_external_legacy_func_table table = {
        external_legacy_implementation::destroy__,
        external_legacy_implementation::call1,
        external_legacy_implementation::call2,
        external_legacy_implementation::call3
    };

    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    Py_INCREF(handle);
    return PyLong_FromVoidPtr(connector_external_legacy_create__(reinterpret_cast<connector_external_legacy_handle>(handle), table));
}

PyObject* external_legacy::module__()
{
    if (PyType_Ready(&call2_callback::type) < 0) {
        return nullptr;
    }
    if (PyType_Ready(&call3_callback::type) < 0) {
        return nullptr;
    }

    static PyMethodDef methods[] = {
        { "create__", create__, METH_VARARGS, nullptr },
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "external_legacy",
        nullptr,
        -1,
        methods
    };
    auto obj = PyModule_Create(&module);
    return obj;
}
}

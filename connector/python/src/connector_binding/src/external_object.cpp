#include "connector_binding_pch.hpp"

#include "external_object.hpp"

#include "details/value.hpp"
#include "details/value_converter.hpp"

namespace connector::binding {
struct external_object_implementation {
    static void destroy__(connector_external_object_handle self)
    {
        auto ptr = reinterpret_cast<PyObject*>(self);
        Py_DECREF(ptr);
    }

    static connector_value callMethod(connector_external_object_handle self, uint32_t id, size_t argc, const connector_value* argv)
    {
        auto ptr = reinterpret_cast<PyObject*>(self);
        auto list = PyList_New(argc);
        for (size_t i = 0; i < argc; ++i) {
            auto pyResult = details::value_converter(argv[i]);
            PyList_SetItem(list, i, pyResult.release());
        }

        PyObject* result = PyObject_CallMethod(ptr, "callMethod", "(O,O)", PyLong_FromLong(id), list);
        return details::value_converter(details::value { result });
    }
};

PyObject* external_object::create__(PyObject* self, PyObject* args)
{
    static connector_external_object_func_table table = {
        &external_object_implementation::destroy__,
        &external_object_implementation::callMethod
    };

    PyObject* handle;
    if (!PyArg_ParseTuple(args, "O", &handle)) {
        return nullptr;
    }
    Py_INCREF(handle);
    return PyLong_FromVoidPtr(connector_external_object_create__(reinterpret_cast<connector_external_object_handle>(handle), table));
}

PyObject* external_object::module__()
{
    static PyMethodDef methods[] = {
        { "create__", create__, METH_VARARGS, nullptr },
        { nullptr, nullptr, 0, nullptr }
    };

    static PyModuleDef module = {
        PyModuleDef_HEAD_INIT,
        "external_object",
        nullptr,
        -1,
        methods
    };
    auto obj = PyModule_Create(&module);
    return obj;
}
}

#include <connector/connector_pch.h>

#include <connector/legacy.h>

#include <connector/core/legacy.hpp>

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT connector_legacy_handle connector_legacy_create__()
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy>();
    return reinterpret_cast<connector_legacy_handle>(ptr.detach());
}

CONNECTOR_EXPORT void connector_legacy_ref__(connector_legacy_handle self)
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy>(reinterpret_cast<connector::core::legacy*>(self));
    ptr.detach();
}

CONNECTOR_EXPORT void connector_legacy_unref__(connector_legacy_handle self)
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy, false>(reinterpret_cast<connector::core::legacy*>(self));
}

CONNECTOR_EXPORT void connector_legacy_call1(connector_legacy_handle self)
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy>(reinterpret_cast<connector::core::legacy*>(self));
    ptr->call();
}

CONNECTOR_EXPORT void connector_legacy_call2(connector_legacy_handle self, connector_legacy_call2_callback_type callback)
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy>(reinterpret_cast<connector::core::legacy*>(self));
    ptr->call(
        [&]() {
            callback.callback(callback.userdata);
        });
    if (callback.userdata_destructor != nullptr) {
        callback.userdata_destructor(callback.userdata);
    }
}

CONNECTOR_EXPORT int64_t connector_legacy_call3(connector_legacy_handle self, connector_legacy_call3_callback_type callback)
{
    auto ptr = connector::core::make_intrusive<connector::core::legacy>(reinterpret_cast<connector::core::legacy*>(self));
    auto res = ptr->call(
        [&](const std::vector<int64_t>& args) -> int64_t {
            return callback.callback(callback.userdata, args.size(), args.data());
        });
    if (callback.userdata_destructor != nullptr) {
        callback.userdata_destructor(callback.userdata);
    }
    return res;
}

#ifdef __cplusplus
}
#endif

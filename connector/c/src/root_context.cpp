#include <connector/connector_pch.h>

#include <connector/root_context.h>

#include <connector/core/root_context.hpp>

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT connector_object_handle connector_root_context_get(const char* path)
{
    auto obj = connector::core::root_context::get(path);
    return reinterpret_cast<connector_object_handle>(obj.detach());
}

CONNECTOR_EXPORT void connector_root_context_registerObject(const char* path, connector_object_handle service)
{
    connector::core::root_context::registerObject(path, connector::core::make_intrusive<connector::core::object>(reinterpret_cast<connector::core::object*>(service)));
}

CONNECTOR_EXPORT void connector_root_context_removeObject(const char* path)
{
    connector::core::root_context::removeObject(path);
}

CONNECTOR_EXPORT connector_object_handle connector_root_context_create(const char* path)
{
    auto obj = connector::core::root_context::create(path);
    return reinterpret_cast<connector_object_handle>(obj.detach());
}

CONNECTOR_EXPORT void connector_root_context_registerObjectFactory(const char* path, connector_root_context_registerObjectFactory_callback_type factory)
{
    auto deleter = [](connector_root_context_registerObjectFactory_callback_type* p) {
        if (p->userdata_destructor != nullptr) {
            p->userdata_destructor(p->userdata);
        }
    };

    auto fact = [f = std::shared_ptr<connector_root_context_registerObjectFactory_callback_type>(new connector_root_context_registerObjectFactory_callback_type(std::move(factory)), deleter)]() {
        return connector::core::make_intrusive<connector::core::object>(reinterpret_cast<connector::core::object*>(f->callback(f->userdata)));
    };
    connector::core::root_context::registerObjectFactory(path, std::move(fact));
}

CONNECTOR_EXPORT void connector_root_context_removeObjectFactory(const char* path)
{
    connector::core::root_context::removeObjectFactory(path);
}

#ifdef __cplusplus
}
#endif

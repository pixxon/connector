#include <connector/connector_pch.h>

#include <connector/external_legacy.h>

#include <connector/core/legacy.hpp>

namespace connector::runtime::details {
struct external_legacy
    : connector::core::legacy {
    external_legacy(connector_external_legacy_handle self, connector_external_legacy_func_table table)
        : self(self)
        , table(table)
    {
    }

    ~external_legacy()
    {
        table.destroy__(self);
    }

    void call() const override
    {
        table.call1(self);
    }

    void call(std::function<void()> callback) const override
    {
        connector_legacy_call2_callback_type cb;
        cb.userdata = &callback;
        cb.callback = [](void* userdata) {
            static_cast<std::function<void()>*>(userdata)->operator()();
        };
        cb.userdata_destructor = nullptr;
        table.call2(self, cb);
    }

    int64_t call(std::function<int64_t(const std::vector<int64_t>&)> callback) const override
    {
        connector_legacy_call3_callback_type cb;
        cb.userdata = &callback;
        cb.callback = [](void* userdata, size_t argc, const int64_t* argv) -> int64_t {
            return static_cast<std::function<int64_t(const std::vector<int64_t>&)>*>(userdata)->operator()({ argv, argv + argc });
        };
        cb.userdata_destructor = nullptr;
        return table.call3(self, cb);
    }

private:
    connector_external_legacy_handle self;
    connector_external_legacy_func_table table;
};
}

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT connector_legacy_handle connector_external_legacy_create__(connector_external_legacy_handle self, connector_external_legacy_func_table table)
{
    auto ptr = connector::core::make_intrusive<connector::runtime::details::external_legacy>(self, table);
    return reinterpret_cast<connector_legacy_handle>(ptr.detach());
}

#ifdef __cplusplus
}
#endif

#include <connector/connector_pch.h>

#include "value_converter.hpp"

#include <connector/core/object.hpp>

namespace connector::details {
connector_value value_converter(const connector::core::value& v)
{
    if (v.isBool()) {
        return connector_value_createBool__(v.getBool());
    } else if (v.isInt32()) {
        return connector_value_createInt32__(v.getInt32());
    } else if (v.isObject()) {
        return connector_value_createObject__(reinterpret_cast<connector_object_handle>(v.getObject().detach()));
    }
    throw std::runtime_error { "invalid value type!" };
}

connector::core::value value_converter(connector_value v)
{
    if (connector_value_isBool(v)) {
        return connector::core::value { connector_value_getBool(v) };
    } else if (connector_value_isInt32(v)) {
        return connector::core::value { connector_value_getInt32(v) };
    } else if (connector_value_isObject(v)) {
        return connector::core::value { connector::core::make_intrusive<connector::core::object, false>(reinterpret_cast<connector::core::object*>(connector_value_getObject(v))) };
    }
    throw std::runtime_error { "invalid value type!" };
}
}

#pragma once

#include <connector/connector_pch.h>

#include <connector/core/value.hpp>
#include <connector/value.h>

namespace connector::details {
connector_value value_converter(const connector::core::value&);
connector::core::value value_converter(connector_value);
}

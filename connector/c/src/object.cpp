#include <connector/connector_pch.h>

#include <connector/object.h>

#include <connector/core/object.hpp>

#include "details/value_converter.hpp"

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT void connector_object_ref__(connector_object_handle self)
{
    auto ptr = connector::core::make_intrusive<connector::core::object>(reinterpret_cast<connector::core::object*>(self));
    ptr.detach();
}

CONNECTOR_EXPORT void connector_object_unref__(connector_object_handle self)
{
    auto ptr = connector::core::make_intrusive<connector::core::object, false>(reinterpret_cast<connector::core::object*>(self));
}

CONNECTOR_EXPORT connector_value connector_object_callMethod(connector_object_handle self, uint32_t name, size_t argc, const connector_value* argv)
{
    auto ptr = connector::core::make_intrusive<connector::core::object>(reinterpret_cast<connector::core::object*>(self));
    std::vector<connector::core::value> args;
    for (auto i = 0u; i < argc; ++i) {
        args.push_back(connector::details::value_converter(argv[i]));
    }
    return connector::details::value_converter(ptr->callMethod(name, args));
}

#ifdef __cplusplus
}
#endif

#include <connector/connector_pch.h>

#include <connector/object.h>
#include <connector/value.h>

#include <connector/core/value.hpp>

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT connector_value connector_value_createBool__(bool b)
{
    connector_value result {
        .type = connector_value_type_BOOL,
        .data = (intptr_t)(b)
    };
    return result;
}

CONNECTOR_EXPORT connector_value connector_value_createObject__(connector_object_handle o)
{
    connector_value result {
        .type = connector_value_type_OBJECT,
        .data = (intptr_t)(o)
    };
    return result;
}

CONNECTOR_EXPORT connector_value connector_value_createInt32__(int32_t i)
{
    connector_value result {
        .type = connector_value_type_INT32,
        .data = (intptr_t)(i)
    };
    return result;
}

CONNECTOR_EXPORT void connector_value_destroy__(connector_value v)
{
    switch (v.type) {
    case connector_value_type_OBJECT:
        connector_object_unref__((connector_object_handle)(v.data));
        break;
    case connector_value_type_INT32:
    case connector_value_type_BOOL:
        break;
    }
}

CONNECTOR_EXPORT bool connector_value_isBool(connector_value v)
{
    return v.type == connector_value_type_BOOL;
}

CONNECTOR_EXPORT bool connector_value_getBool(connector_value v)
{
    return (bool)(v.data);
}

CONNECTOR_EXPORT bool connector_value_isObject(connector_value v)
{
    return v.type == connector_value_type_OBJECT;
}

CONNECTOR_EXPORT connector_object_handle connector_value_getObject(connector_value v)
{
    connector_object_ref__((connector_object_handle)(v.data));
    return (connector_object_handle)(v.data);
}

CONNECTOR_EXPORT bool connector_value_isInt32(connector_value v)
{
    return v.type == connector_value_type_INT32;
}

CONNECTOR_EXPORT int32_t connector_value_getInt32(connector_value v)
{
    return (int32_t)(v.data);
}

#ifdef __cplusplus
}
#endif

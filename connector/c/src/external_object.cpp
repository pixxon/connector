#include <connector/connector_pch.h>

#include <connector/external_object.h>

#include <connector/core/object.hpp>

#include "details/value_converter.hpp"

namespace connector::details {
struct external_object
    : connector::core::object {
    external_object(connector_external_object_handle self, connector_external_object_func_table table)
        : self_(self)
        , table_(table)
    {
    }

    ~external_object()
    {
        table_.destroy__(self_);
    }

    connector::core::value callMethod(uint32_t id, const std::vector<connector::core::value>& args) final
    {
        std::vector<connector_value> cArgs;
        for (const auto& v : args) {
            cArgs.push_back(value_converter(v));
        }
        auto cResult = table_.callMethod(self_, id, cArgs.size(), cArgs.data());

        auto result = value_converter(cResult);

        for (auto& v : cArgs) {
            connector_value_destroy__(v);
        }
        connector_value_destroy__(cResult);
        return result;
    }

private:
    connector_external_object_handle self_;
    connector_external_object_func_table table_;
};
}

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT connector_object_handle connector_external_object_create__(connector_external_object_handle self, connector_external_object_func_table table)
{
    auto ptr = connector::core::make_intrusive<connector::details::external_object>(self, table);
    return reinterpret_cast<connector_object_handle>(ptr.detach());
}

#ifdef __cplusplus
}
#endif

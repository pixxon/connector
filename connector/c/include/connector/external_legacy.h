#pragma once

#include <connector/connector_pch.h>

#include <connector/legacy.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct connector_external_legacy_handle__* connector_external_legacy_handle;

typedef void (*connector_external_legacy_destroy__)(connector_external_legacy_handle);
typedef void (*connector_external_legacy_call1)(connector_external_legacy_handle);
typedef void (*connector_external_legacy_call2)(connector_external_legacy_handle, connector_legacy_call2_callback_type);
typedef int64_t (*connector_external_legacy_call3)(connector_external_legacy_handle, connector_legacy_call3_callback_type);

typedef struct connector_external_legacy_func_table__ {
    connector_external_legacy_destroy__ destroy__;
    connector_external_legacy_call1 call1;
    connector_external_legacy_call2 call2;
    connector_external_legacy_call3 call3;
} connector_external_legacy_func_table;

CONNECTOR_EXPORT connector_legacy_handle connector_external_legacy_create__(connector_external_legacy_handle self, connector_external_legacy_func_table table);

#ifdef __cplusplus
}
#endif

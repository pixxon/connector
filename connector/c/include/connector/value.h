#pragma once

#include <connector/connector_pch.h>

#include <connector/object_fwd.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum connector_value_type__ {
    connector_value_type_OBJECT = 0,
    connector_value_type_INT32 = 1,
    connector_value_type_BOOL = 2
} connector_value_type;

typedef struct connector_value__ {
    connector_value_type type;
    intptr_t data;
} connector_value;

CONNECTOR_EXPORT connector_value connector_value_createBool__(bool);
CONNECTOR_EXPORT connector_value connector_value_createObject__(connector_object_handle);
CONNECTOR_EXPORT connector_value connector_value_createInt32__(int32_t);

CONNECTOR_EXPORT void connector_value_destroy__(connector_value);

CONNECTOR_EXPORT bool connector_value_isBool(connector_value);
CONNECTOR_EXPORT bool connector_value_getBool(connector_value);
CONNECTOR_EXPORT bool connector_value_isObject(connector_value);
CONNECTOR_EXPORT connector_object_handle connector_value_getObject(connector_value);
CONNECTOR_EXPORT bool connector_value_isInt32(connector_value);
CONNECTOR_EXPORT int32_t connector_value_getInt32(connector_value);

#ifdef __cplusplus
}
#endif

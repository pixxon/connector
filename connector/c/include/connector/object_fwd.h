#pragma once

#include <connector/connector_pch.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct connector_object_handle__* connector_object_handle;

#ifdef __cplusplus
}
#endif

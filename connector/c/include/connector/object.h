#pragma once

#include <connector/connector_pch.h>

#include <connector/object_fwd.h>
#include <connector/value.h>

#ifdef __cplusplus
extern "C" {
#endif

CONNECTOR_EXPORT void connector_object_ref__(connector_object_handle self);
CONNECTOR_EXPORT void connector_object_unref__(connector_object_handle self);

CONNECTOR_EXPORT connector_value connector_object_callMethod(connector_object_handle self, uint32_t name, size_t argc, const connector_value* argv);

#ifdef __cplusplus
}
#endif

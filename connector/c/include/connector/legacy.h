#pragma once

#include <connector/connector_pch.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct connector_legacy_handle__* connector_legacy_handle;

typedef struct connector_legacy_call2_callback_type__ {
    void* userdata;
    void (*userdata_destructor)(void*);
    void (*callback)(void*);
} connector_legacy_call2_callback_type;

typedef struct connector_legacy_call3_callback_type__ {
    void* userdata;
    void (*userdata_destructor)(void*);
    int64_t (*callback)(void*, size_t, const int64_t*);
} connector_legacy_call3_callback_type;

CONNECTOR_EXPORT connector_legacy_handle connector_legacy_create__();

CONNECTOR_EXPORT void connector_legacy_ref__(connector_legacy_handle self);
CONNECTOR_EXPORT void connector_legacy_unref__(connector_legacy_handle self);

CONNECTOR_EXPORT void connector_legacy_call1(connector_legacy_handle self);
CONNECTOR_EXPORT void connector_legacy_call2(connector_legacy_handle self, connector_legacy_call2_callback_type callback);
CONNECTOR_EXPORT int64_t connector_legacy_call3(connector_legacy_handle self, connector_legacy_call3_callback_type callback);

#ifdef __cplusplus
}
#endif

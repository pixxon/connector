#pragma once

#include <connector/connector_pch.h>

#include <connector/object.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct connector_external_object_handle__* connector_external_object_handle;

typedef void (*connector_external_object_destroy__)(connector_external_object_handle);
typedef connector_value (*connector_external_object_callMethod)(connector_external_object_handle, uint32_t, size_t, const connector_value*);

typedef struct connector_external_object_func_table__ {
    connector_external_object_destroy__ destroy__;
    connector_external_object_callMethod callMethod;
} connector_external_object_func_table;

CONNECTOR_EXPORT connector_object_handle connector_external_object_create__(connector_external_object_handle self, connector_external_object_func_table table);

#ifdef __cplusplus
}
#endif

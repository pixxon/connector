#pragma once

#include <connector/connector_pch.h>

#include <connector/object.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct connector_root_context_registerObjectFactory_callback_type__ {
    void* userdata;
    void (*userdata_destructor)(void*);
    connector_object_handle (*callback)(void*);
} connector_root_context_registerObjectFactory_callback_type;

CONNECTOR_EXPORT connector_object_handle connector_root_context_get(const char* path);
CONNECTOR_EXPORT void connector_root_context_registerObject(const char* path, connector_object_handle service);
CONNECTOR_EXPORT void connector_root_context_removeObject(const char* path);
CONNECTOR_EXPORT connector_object_handle connector_root_context_create(const char* path);
CONNECTOR_EXPORT void connector_root_context_registerObjectFactory(const char* path, connector_root_context_registerObjectFactory_callback_type factory);
CONNECTOR_EXPORT void connector_root_context_removeObjectFactory(const char* path);

#ifdef __cplusplus
}
#endif

#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define CONNECTOR_EXPORT __attribute__((visibility("default")))

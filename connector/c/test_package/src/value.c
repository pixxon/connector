#include "value.h"

#include "external_object.h"

START_TEST(test_int32)
{
    connector_value v = connector_value_createInt32__(5);
    ck_assert(connector_value_isInt32(v));
    ck_assert_int_eq(5, connector_value_getInt32(v));
    connector_value_destroy__(v);
}
END_TEST

START_TEST(test_bool)
{
    connector_value v = connector_value_createBool__(true);
    ck_assert(connector_value_isBool(v));
    ck_assert_int_eq(true, connector_value_getBool(v));
    connector_value_destroy__(v);
}
END_TEST

START_TEST(test_object)
{
    test_external_object_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_object_func_table table = {
        .destroy__ = &test_connector_external_object_destroy__,
        .callMethod = &test_connector_external_object_callMethod
    };
    connector_object_handle k1 = connector_external_object_create__((connector_external_object_handle)&data, table);
    // we give up ownership of k1 here
    connector_value v = connector_value_createObject__(k1);
    connector_object_handle k2 = connector_value_getObject(v);
    ck_assert_ptr_eq(k1, k2);
    connector_object_unref__(k2);
    connector_value_destroy__(v);
    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

Suite* value_suite(void)
{
    TCase* tc_test_int32 = tcase_create("test_int32");
    tcase_add_test(tc_test_int32, test_int32);
    TCase* tc_test_bool = tcase_create("test_bool");
    tcase_add_test(tc_test_bool, test_bool);
    TCase* tc_test_object = tcase_create("test_object");
    tcase_add_test(tc_test_object, test_object);

    Suite* ts_value = suite_create("value");
    suite_add_tcase(ts_value, tc_test_int32);
    suite_add_tcase(ts_value, tc_test_bool);
    suite_add_tcase(ts_value, tc_test_object);

    return ts_value;
}

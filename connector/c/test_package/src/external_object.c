#include "external_object.h"

#include <stdlib.h>

void test_connector_external_object_destroy__(connector_external_object_handle handle)
{
    (*(test_external_object_handle*)handle).destroyed = 1;
}

connector_value test_connector_external_object_callMethod(connector_external_object_handle handle, uint32_t, size_t, const connector_value*)
{
    (*(test_external_object_handle*)handle).called = 1;
    return connector_value_createInt32__(0);
}

START_TEST(test_callMethod)
{
    test_external_object_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_object_func_table table = {
        .destroy__ = &test_connector_external_object_destroy__,
        .callMethod = &test_connector_external_object_callMethod
    };
    connector_object_handle k = connector_external_object_create__((connector_external_object_handle)&data, table);
    connector_value result = connector_object_callMethod(k, 0, 0, NULL);
    connector_value_destroy__(result);

    ck_assert_int_eq(1, data.called);

    connector_object_unref__(k);

    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

Suite* external_object_suite(void)
{
    TCase* tc_test_callMethod = tcase_create("test_callMethod");
    tcase_add_test(tc_test_callMethod, test_callMethod);

    Suite* ts_external_object = suite_create("external_object");
    suite_add_tcase(ts_external_object, tc_test_callMethod);

    return ts_external_object;
}

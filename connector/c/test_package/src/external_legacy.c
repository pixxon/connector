#include "external_legacy.h"
#include "legacy.h"

#include <stdlib.h>

void test_connector_external_legacy_destroy__(connector_external_legacy_handle handle)
{
    (*(test_external_legacy_handle*)handle).destroyed = 1;
}

void test_connector_external_legacy_call1(connector_external_legacy_handle handle)
{
    (*(test_external_legacy_handle*)handle).called = 1;
}

void test_connector_external_legacy_call2(connector_external_legacy_handle handle, connector_legacy_call2_callback_type callback)
{
    (*(test_external_legacy_handle*)handle).called = 1;
    callback.callback(callback.userdata);
    if (callback.userdata_destructor != NULL) {
        callback.userdata_destructor(callback.userdata);
    }
}

int64_t test_connector_external_legacy_call3(connector_external_legacy_handle handle, connector_legacy_call3_callback_type callback)
{
    (*(test_external_legacy_handle*)handle).called = 1;
    int64_t args[2] = { 3, 4 };
    int64_t result = callback.callback(callback.userdata, 2, args);
    if (callback.userdata_destructor != NULL) {
        callback.userdata_destructor(callback.userdata);
    }
    return result * 2;
}

START_TEST(test_call1)
{
    test_external_legacy_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_legacy_func_table table = {
        .destroy__ = &test_connector_external_legacy_destroy__,
        .call1 = &test_connector_external_legacy_call1
    };
    connector_legacy_handle k = connector_external_legacy_create__((connector_external_legacy_handle)&data, table);
    connector_legacy_call1(k);

    ck_assert_int_eq(1, data.called);

    connector_legacy_unref__(k);

    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

START_TEST(test_call2)
{
    int32_t userdata = 0;

    test_external_legacy_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_legacy_func_table table = {
        .destroy__ = &test_connector_external_legacy_destroy__,
        .call2 = &test_connector_external_legacy_call2
    };
    connector_legacy_handle k = connector_external_legacy_create__((connector_external_legacy_handle)&data, table);

    connector_legacy_call2_callback_type cb;
    cb.userdata = (void*)&userdata;
    cb.userdata_destructor = NULL;
    cb.callback = &test_connector_legacy_call2_callback;

    connector_legacy_call2(k, cb);
    ck_assert_int_eq(1, data.called);
    ck_assert_int_eq(1, userdata);

    connector_legacy_unref__(k);

    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

START_TEST(test_call3)
{
    test_external_legacy_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_legacy_func_table table = {
        .destroy__ = &test_connector_external_legacy_destroy__,
        .call3 = &test_connector_external_legacy_call3
    };
    connector_legacy_handle k = connector_external_legacy_create__((connector_external_legacy_handle)&data, table);

    connector_legacy_call3_callback_type cb;
    cb.userdata = NULL;
    cb.userdata_destructor = NULL;
    cb.callback = &test_connector_legacy_call3_callback;

    int64_t result = connector_legacy_call3(k, cb);
    ck_assert_int_eq(1, data.called);
    ck_assert_int_eq(14, result);

    connector_legacy_unref__(k);

    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

Suite* external_legacy_suite(void)
{
    TCase* tc_test_call1 = tcase_create("test_call1");
    tcase_add_test(tc_test_call1, test_call1);

    TCase* tc_test_call2 = tcase_create("test_call2");
    tcase_add_test(tc_test_call2, test_call2);

    TCase* tc_test_call3 = tcase_create("test_call3");
    tcase_add_test(tc_test_call3, test_call3);

    Suite* ts_external_legacy = suite_create("external_legacy");
    suite_add_tcase(ts_external_legacy, tc_test_call1);
    suite_add_tcase(ts_external_legacy, tc_test_call2);
    suite_add_tcase(ts_external_legacy, tc_test_call3);

    return ts_external_legacy;
}

#include "external_legacy.h"
#include "external_object.h"
#include "legacy.h"
#include "root_context.h"
#include "value.h"

#include <check.h>

#include <stdlib.h>

int main(void)
{
    SRunner* sr = srunner_create(NULL);
    srunner_add_suite(sr, legacy_suite());
    srunner_add_suite(sr, external_legacy_suite());
    srunner_add_suite(sr, external_object_suite());
    srunner_add_suite(sr, root_context_suite());
    srunner_add_suite(sr, value_suite());

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

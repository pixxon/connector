#include "root_context.h"

#include "external_object.h"

#include <stdlib.h>

START_TEST(test_noObject)
{
    connector_object_handle obj = connector_root_context_get("calculator");
    ck_assert_ptr_null(obj);
}
END_TEST

START_TEST(test_hasObject)
{
    test_external_object_handle data = {
        .destroyed = 0,
        .called = 0
    };
    connector_external_object_func_table table = {
        .destroy__ = &test_connector_external_object_destroy__
    };
    connector_object_handle k = connector_external_object_create__((connector_external_object_handle)&data, table);
    connector_root_context_registerObject("calculator", k);

    connector_object_handle obj = connector_root_context_get("calculator");
    ck_assert_ptr_nonnull(obj);
    ck_assert_ptr_eq(obj, k);
    connector_object_unref__(k);
    connector_object_unref__(obj);

    ck_assert_int_eq(0, data.destroyed);

    connector_root_context_removeObject("calculator");

    ck_assert_int_eq(1, data.destroyed);
}
END_TEST

Suite* root_context_suite(void)
{
    TCase* tc_test_noObject = tcase_create("test_noObject");
    tcase_add_test(tc_test_noObject, test_noObject);

    TCase* tc_test_hasObject = tcase_create("test_hasObject");
    tcase_add_test(tc_test_hasObject, test_hasObject);

    Suite* ts_root_context = suite_create("root_context");
    suite_add_tcase(ts_root_context, tc_test_noObject);
    suite_add_tcase(ts_root_context, tc_test_hasObject);

    return ts_root_context;
}

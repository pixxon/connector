#include "legacy.h"

void test_connector_legacy_call2_callback(void* userdata)
{
    (*(int32_t*)userdata) = 1;
}

int64_t test_connector_legacy_call3_callback(void* userdata, size_t argc, const int64_t* argv)
{
    int64_t sum = 0;
    for (const int64_t* i = argv; i != (argv + argc); ++i) {
        sum += (*i);
    }
    return sum;
}

START_TEST(test_call1)
{
    connector_legacy_handle k = connector_legacy_create__();
    connector_legacy_call1(k);
    connector_legacy_unref__(k);
}
END_TEST

START_TEST(test_call2)
{
    int32_t userdata = 0;

    connector_legacy_handle k = connector_legacy_create__();

    connector_legacy_call2_callback_type cb;
    cb.userdata = (void*)&userdata;
    cb.userdata_destructor = NULL;
    cb.callback = &test_connector_legacy_call2_callback;

    connector_legacy_call2(k, cb);
    ck_assert_int_eq(1, userdata);

    connector_legacy_unref__(k);
}
END_TEST

START_TEST(test_call3)
{
    connector_legacy_handle k = connector_legacy_create__();

    connector_legacy_call3_callback_type cb;
    cb.userdata = NULL;
    cb.userdata_destructor = NULL;
    cb.callback = &test_connector_legacy_call3_callback;

    int64_t result = connector_legacy_call3(k, cb);
    ck_assert_int_eq(14, result);

    connector_legacy_unref__(k);
}
END_TEST

Suite* legacy_suite(void)
{
    TCase* tc_test_call1 = tcase_create("test_call1");
    tcase_add_test(tc_test_call1, test_call1);

    TCase* tc_test_call2 = tcase_create("test_call2");
    tcase_add_test(tc_test_call2, test_call2);

    TCase* tc_test_call3 = tcase_create("test_call3");
    tcase_add_test(tc_test_call3, test_call3);

    Suite* ts_legacy = suite_create("legacy");
    suite_add_tcase(ts_legacy, tc_test_call1);
    suite_add_tcase(ts_legacy, tc_test_call2);
    suite_add_tcase(ts_legacy, tc_test_call3);

    return ts_legacy;
}

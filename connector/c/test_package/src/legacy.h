#pragma once

#include <connector/legacy.h>

#include <check.h>

Suite* legacy_suite(void);

void test_connector_legacy_call2_callback(void* userdata);
int64_t test_connector_legacy_call3_callback(void* userdata, size_t argc, const int64_t* argv);

#pragma once

#include <connector/external_legacy.h>

#include <check.h>

Suite* external_legacy_suite(void);

typedef struct test_external_legacy_handle__ {
    int destroyed;
    int called;
} test_external_legacy_handle;

void test_connector_external_legacy_destroy__(connector_external_legacy_handle handle);
void test_connector_external_legacy_call1(connector_external_legacy_handle handle);
void test_connector_external_legacy_call2(connector_external_legacy_handle handle, connector_legacy_call2_callback_type callback);
int64_t test_connector_external_legacy_call3(connector_external_legacy_handle handle, connector_legacy_call3_callback_type callback);

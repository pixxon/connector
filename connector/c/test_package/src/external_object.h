#pragma once

#include <connector/external_object.h>

#include <check.h>

Suite* external_object_suite(void);

typedef struct test_external_object_handle__ {
    int destroyed;
    int called;
} test_external_object_handle;

void test_connector_external_object_destroy__(connector_external_object_handle handle);
connector_value test_connector_external_object_callMethod(connector_external_object_handle handle, uint32_t, size_t, const connector_value*);

#pragma once

#include <connector/connector_pch.hpp>

#include <connector/object.h>
#include <connector/value.hpp>

namespace connector {
struct object {
    explicit object(const object&);
    explicit object(object&&);
    explicit object(connector_object_handle handle, bool addRef = true);

    virtual ~object();

    object& operator=(const object&);
    object& operator=(object&&);

    connector_object_handle handle() const;

    virtual value callMethod(uint32_t id, const std::vector<value>& args);

protected:
    connector_object_handle handle_;
};
}

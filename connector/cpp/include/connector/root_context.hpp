#pragma once

#include <connector/connector_pch.hpp>

#include <connector/object.hpp>

#include <connector/root_context.h>

namespace connector {
struct root_context {
    static std::shared_ptr<object> get(std::string path);
    static void registerObject(std::string path, std::shared_ptr<object>);
    static void removeObject(std::string path);

    //		static std::shared_ptr<object> create(std::string path);
    //		static void registerObjectFactory(std::string path, std::function<std::shared_ptr<object>()>);
    //		static void removeObjectFactory(std::string path);
};
}

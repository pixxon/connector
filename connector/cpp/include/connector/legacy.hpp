#pragma once

#include <connector/connector_pch.hpp>

#include <connector/legacy.h>

namespace connector {
struct legacy {
    explicit legacy();
    explicit legacy(const legacy&);
    explicit legacy(legacy&&);
    explicit legacy(connector_legacy_handle handle, bool addRef = true);

    virtual ~legacy();

    legacy& operator=(const legacy&);
    legacy& operator=(legacy&&);

    virtual void call() const;
    virtual void call(std::function<void()>) const;
    virtual int64_t call(std::function<int64_t(const std::vector<int64_t>&)>) const;

protected:
    connector_legacy_handle handle_;
};
}

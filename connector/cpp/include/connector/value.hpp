#pragma once

#include <connector/connector_pch.hpp>

namespace connector {
struct object;

struct value {
    enum class type {
        OBJECT,
        INT32,
        BOOL
    };

    explicit value(std::shared_ptr<object>);
    explicit value(int32_t);
    explicit value(bool);

    ~value();

    bool isBool() const;
    bool getBool() const;

    bool isObject() const;
    std::shared_ptr<object> getObject() const;

    bool isInt32() const;
    int32_t getInt32() const;

    friend bool operator==(const value&, const value&);

private:
    intptr_t data_;
    type t_;
};

}

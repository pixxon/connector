#pragma once

#include <connector/connector_pch.hpp>

#include <connector/legacy.hpp>

#include <connector/external_legacy.h>

namespace connector {
struct external_legacy
    : legacy {
    struct implementation {
        void call() const;
        void call(std::function<void()>) const;
        int64_t call(std::function<int64_t(const std::vector<int64_t>&)>) const;
    };

    external_legacy(implementation*);
    external_legacy(const external_legacy&);
    external_legacy(external_legacy&&);

    ~external_legacy();

    external_legacy& operator=(const external_legacy&);
    external_legacy& operator=(external_legacy&&);

    void call() const override;
    void call(std::function<void()>) const override;
    int64_t call(std::function<int64_t(const std::vector<int64_t>&)>) const override;

private:
    static void destroy__(connector_external_legacy_handle self);
    static void call1(connector_external_legacy_handle self);
    static void call2(connector_external_legacy_handle self, connector_legacy_call2_callback_type);
    static int64_t call3(connector_external_legacy_handle self, connector_legacy_call3_callback_type);

    static connector_external_legacy_func_table table__;

    implementation* impl_;
};
}

#pragma once

#include <connector/connector_pch.hpp>

#include <connector/object.hpp>

#include <connector/external_object.h>

namespace connector {
struct external_object
    : object {
    struct implementation {
        virtual value callMethod(uint32_t id, const std::vector<value>& args) = 0;
        virtual ~implementation() = default;
    };

    external_object(implementation*);
    external_object(const external_object&);
    external_object(external_object&&);

    virtual ~external_object();

    external_object& operator=(const external_object&);
    external_object& operator=(external_object&&);

    value callMethod(uint32_t id, const std::vector<value>& args) override;

private:
    static void destroy__(connector_external_object_handle self);
    static connector_value callMethod(connector_external_object_handle self, uint32_t id, size_t argc, const connector_value* argv);

    static connector_external_object_func_table table__;

    implementation* impl_;
};
}

#include <connector/connector_pch.hpp>

#include <connector/object.hpp>
#include <connector/value.hpp>

namespace connector {
value::value(std::shared_ptr<object> obj)
    : data_ { reinterpret_cast<intptr_t>(new std::shared_ptr<object>(std::move(obj))) }
    , t_ { type::OBJECT }
{
}

value::value(int32_t i)
    : data_ { static_cast<intptr_t>(i) }
    , t_ { type::INT32 }
{
}

value::value(bool b)
    : data_ { static_cast<intptr_t>(b) }
    , t_ { type::BOOL }
{
}

bool value::isBool() const
{
    return t_ == type::BOOL;
}

bool value::getBool() const
{
    return static_cast<bool>(data_);
}

bool value::isObject() const
{
    return t_ == type::OBJECT;
}

std::shared_ptr<object> value::getObject() const
{
    return *reinterpret_cast<std::shared_ptr<object>*>(data_);
}

bool value::isInt32() const
{
    return t_ == type::INT32;
}

int32_t value::getInt32() const
{
    return static_cast<int32_t>(data_);
}

value::~value()
{
    switch (t_) {
    case type::OBJECT:
        delete reinterpret_cast<std::shared_ptr<object>*>(data_);
        break;
    case type::INT32:
    case type::BOOL:
        break;
    }
}

bool operator==(const value& a, const value& b)
{
    return a.t_ == b.t_ && a.data_ == b.data_;
}
}

#include <connector/connector_pch.hpp>

#include <connector/root_context.hpp>

namespace connector {
std::shared_ptr<object> root_context::get(std::string path)
{
    return std::make_shared<object>(connector_root_context_get(path.c_str()), false);
}

void root_context::registerObject(std::string path, std::shared_ptr<object> obj)
{
    connector_root_context_registerObject(path.c_str(), reinterpret_cast<connector_object_handle>(obj->handle()));
}

void root_context::removeObject(std::string path)
{
    connector_root_context_removeObject(path.c_str());
}
}

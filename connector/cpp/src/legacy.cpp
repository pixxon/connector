#include <connector/connector_pch.hpp>

#include <connector/legacy.hpp>

namespace connector {
legacy::legacy()
    : handle_ { connector_legacy_create__() }
{
}

legacy::legacy(const legacy& o)
    : handle_ { o.handle_ }
{
    connector_legacy_ref__(handle_);
}

legacy::legacy(legacy&& o)
    : handle_ { o.handle_ }
{
    o.handle_ = nullptr;
}

legacy::legacy(connector_legacy_handle handle, bool addRef)
    : handle_ { handle }
{
    if (addRef) {
        connector_legacy_ref__(handle_);
    }
}

legacy::~legacy()
{
    connector_legacy_unref__(handle_);
}

legacy& legacy::operator=(const legacy& o)
{
    connector_legacy_unref__(handle_);
    handle_ = o.handle_;
    connector_legacy_ref__(handle_);
    return *this;
}

legacy& legacy::operator=(legacy&& o)
{
    connector_legacy_unref__(handle_);
    handle_ = o.handle_;
    o.handle_ = nullptr;
    return *this;
}

void legacy::call() const
{
    connector_legacy_call1(handle_);
}

void legacy::call(std::function<void()> callback) const
{
    connector_legacy_call2_callback_type cb;
    cb.userdata = &callback;
    cb.callback = [](void* userdata) {
        static_cast<std::function<void()>*>(userdata)->operator()();
    };
    cb.userdata_destructor = nullptr;
    connector_legacy_call2(handle_, cb);
}

int64_t legacy::call(std::function<int64_t(const std::vector<int64_t>&)> callback) const
{
    connector_legacy_call3_callback_type cb;
    cb.userdata = &callback;
    cb.callback = [](void* userdata, size_t argc, const int64_t* argv) -> int64_t {
        return static_cast<std::function<int64_t(const std::vector<int64_t>&)>*>(userdata)->operator()({ argv, argv + argc });
    };
    cb.userdata_destructor = nullptr;
    return connector_legacy_call3(handle_, cb);
}
}

#include <connector/connector_pch.hpp>

#include <connector/external_object.hpp>

namespace connector {
external_object::external_object(implementation* impl)
    : object { connector_external_object_create__(reinterpret_cast<connector_external_object_handle>(impl), table__), false }
    , impl_ { std::move(impl) }
{
}

external_object::external_object(const external_object& o)
    : object { std::forward<const external_object&>(o) }
    , impl_ { o.impl_ }
{
}

external_object::external_object(external_object&& o)
    : object { std::forward<external_object&&>(o) }
    , impl_ { o.impl_ }
{
}

external_object::~external_object()
{
}

external_object& external_object::operator=(const external_object& o)
{
    impl_ = o.impl_;
    static_cast<object&>(*this) = std::forward<const external_object&>(o);
    return *this;
}

external_object& external_object::operator=(external_object&& o)
{
    impl_ = o.impl_;
    static_cast<object&>(*this) = std::forward<external_object&&>(o);
    return *this;
}

value external_object::callMethod(uint32_t id, const std::vector<value>& args)
{
    return impl_->callMethod(id, args);
}

void external_object::destroy__(connector_external_object_handle self)
{
    delete reinterpret_cast<implementation*>(self);
}

connector_value external_object::callMethod(connector_external_object_handle self, uint32_t id, size_t argc, const connector_value* argv)
{
    std::vector<connector::value> args;
    for (auto i = 0u; i < argc; ++i) {
        args.push_back(connector::value { connector_value_getInt32(argv[i]) });
    }
    auto result = reinterpret_cast<implementation*>(self)->callMethod(id, args);
    return connector_value_createInt32__(result.getInt32());
}

connector_external_object_func_table external_object::table__ = {
    destroy__,
    callMethod
};
}

#include <connector/connector_pch.hpp>

#include <connector/object.hpp>

namespace connector {
object::object(const object& o)
    : handle_ { o.handle_ }
{
    connector_object_ref__(handle_);
}

object::object(object&& o)
    : handle_ { o.handle_ }
{
    o.handle_ = nullptr;
}

object::object(connector_object_handle handle, bool addRef)
    : handle_ { handle }
{
    if (addRef) {
        connector_object_ref__(handle_);
    }
}

object::~object()
{
    connector_object_unref__(handle_);
}

object& object::operator=(const object& o)
{
    connector_object_unref__(handle_);
    handle_ = o.handle_;
    connector_object_ref__(handle_);
    return *this;
}

object& object::operator=(object&& o)
{
    connector_object_unref__(handle_);
    handle_ = o.handle_;
    o.handle_ = nullptr;
    return *this;
}

connector_object_handle object::handle() const
{
    return handle_;
}

value object::callMethod(uint32_t id, const std::vector<value>& args)
{
    std::vector<connector_value> cArgs;
    for (const auto& a : args) {
        cArgs.push_back(connector_value_createInt32__(a.getInt32()));
    }
    return value { connector_value_getInt32(connector_object_callMethod(handle_, id, cArgs.size(), cArgs.data())) };
}
}

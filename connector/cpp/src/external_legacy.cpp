#include <connector/connector_pch.hpp>

#include <connector/external_legacy.hpp>

namespace connector {
external_legacy::external_legacy(implementation* impl)
    : legacy { connector_external_legacy_create__(reinterpret_cast<connector_external_legacy_handle>(impl), table__), false }
    , impl_ { std::move(impl) }
{
}

external_legacy::external_legacy(const external_legacy& o)
    : legacy { o }
    , impl_ { o.impl_ }
{
}

external_legacy::external_legacy(external_legacy&& o)
    : legacy { o }
    , impl_ { o.impl_ }
{
}

external_legacy::~external_legacy()
{
}

external_legacy& external_legacy::operator=(const external_legacy& o)
{
    impl_ = o.impl_;
    static_cast<legacy&>(*this) = o;
    return *this;
}

external_legacy& external_legacy::operator=(external_legacy&& o)
{
    impl_ = o.impl_;
    static_cast<legacy&>(*this) = o;
    return *this;
}

void external_legacy::call() const
{
    impl_->call();
}

void external_legacy::call(std::function<void()> f) const
{
    impl_->call(f);
}

int64_t external_legacy::call(std::function<int64_t(const std::vector<int64_t>&)> f) const
{
    return impl_->call(f);
}

void external_legacy::destroy__(connector_external_legacy_handle self)
{
    delete reinterpret_cast<implementation*>(self);
}

void external_legacy::call1(connector_external_legacy_handle self)
{
    reinterpret_cast<implementation*>(self)->call();
}

void external_legacy::call2(connector_external_legacy_handle self, connector_legacy_call2_callback_type f)
{
    reinterpret_cast<implementation*>(self)->call([&]() { f.callback(f.userdata); });
}

int64_t external_legacy::call3(connector_external_legacy_handle self, connector_legacy_call3_callback_type f)
{
    return reinterpret_cast<implementation*>(self)->call(
        [&](const std::vector<int64_t>& d) -> int64_t {
            return f.callback(f.userdata, d.size(), d.data());
        });
}

connector_external_legacy_func_table external_legacy::table__ = {
    destroy__,
    call1,
    call2,
    call3
};

void external_legacy::implementation::call() const
{
}

void external_legacy::implementation::call(std::function<void()> f) const
{
    f();
}

int64_t external_legacy::implementation::call(std::function<int64_t(const std::vector<int64_t>&)> f) const
{
    return f({ 3, 4 }) * 2;
}
}

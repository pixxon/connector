from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain, cmake_layout, CMakeDeps
from conan.tools.build import can_run
from conan.tools.files import copy

class ConnectorCXXTestRecipe(ConanFile):
    settings = "os", "compiler", "build_type", "arch"

    requires = "gtest/1.14.0", "connector/0.0.0"

    build_requires = "cmake/3.30.0", "ninja/1.12.1"

    def requirements(self):
        self.requires(self.tested_reference_str)

    def generate(self):
        deps = CMakeDeps(self)
        deps.generate()
        for dep in self.dependencies.values():
            for dir in dep.cpp_info.libdirs:
                copy(self, "*.dylib", dir, self.build_folder)
                copy(self, "*.dll", dir, self.build_folder)
                copy(self, "*.so", dir, self.build_folder)
        tc = CMakeToolchain(self, generator="Ninja")
        tc.cache_variables["CMAKE_CTEST_ARGUMENTS"] = "--output-on-failure;--output-junit;test_results.xml"
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def layout(self):
        cmake_layout(self)

    def test(self):
        if can_run(self):
            cmake = CMake(self)
            cmake.test()

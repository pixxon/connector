#include <connector/external_object.hpp>
#include <connector/value.hpp>

#include <numeric>

#include <gtest/gtest.h>

TEST(value, Int32)
{
    auto v = connector::value { 3 };
    EXPECT_TRUE(v.isInt32());
    EXPECT_EQ(v.getInt32(), 3);
}

TEST(value, Bool)
{
    auto v = connector::value { true };
    EXPECT_TRUE(v.isBool());
    EXPECT_EQ(v.getBool(), true);
}

TEST(value, Object)
{
    auto obj = std::make_shared<connector::external_object>(nullptr);
    auto v = connector::value { obj };
    EXPECT_TRUE(v.isObject());
    EXPECT_EQ(v.getObject(), obj);
}

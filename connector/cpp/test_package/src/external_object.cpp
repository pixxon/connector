#include <connector/external_object.hpp>

#include <numeric>

#include <gtest/gtest.h>

struct test_implementation
    : connector::external_object::implementation {
    connector::value callMethod(uint32_t id, const std::vector<connector::value>& args) final
    {
        int32_t result = id + args.at(0).getInt32() + args.at(1).getInt32();
        return connector::value { result };
    }
};

TEST(external_object, callMethod)
{
    auto obj = std::make_shared<connector::external_object>(new test_implementation());
    EXPECT_EQ(connector::value { 3 }, obj->callMethod(0, { connector::value { 1 }, connector::value { 2 } }));
}

#include <connector/external_object.hpp>
#include <connector/root_context.hpp>
#include <connector/value.hpp>

#include <numeric>

#include <gtest/gtest.h>

struct test_implementation
    : connector::external_object::implementation {
    connector::value callMethod(uint32_t id, const std::vector<connector::value>& args) final
    {
        int32_t result = id + args.at(0).getInt32() + args.at(1).getInt32();
        return connector::value { result };
    }
};

TEST(root_context, noObject)
{
    auto obj = connector::root_context::get("calculator");
    EXPECT_EQ(nullptr, obj->handle());
}

TEST(root_context, hasObject)
{
    {
        auto obj1 = std::make_shared<connector::external_object>(new test_implementation());
        connector::root_context::registerObject("calculator", obj1);
    }

    {
        auto obj2 = connector::root_context::get("calculator");
        EXPECT_EQ(connector::value { 3 }, obj2->callMethod(0, { connector::value { 1 }, connector::value { 2 } }));
    }

    connector::root_context::removeObject("calculator");

    {
        auto obj = connector::root_context::get("calculator");
        EXPECT_EQ(nullptr, obj->handle());
    }
}
#include <connector/legacy.hpp>

#include <numeric>

#include <gtest/gtest.h>

TEST(legacy, call1)
{
    auto obj = std::make_unique<connector::legacy>();
    obj->call();
}

TEST(legacy, call2)
{
    int count = 0;
    auto callback = [&count]() {
        count++;
    };

    auto obj = std::make_unique<connector::legacy>();
    obj->call(callback);

    EXPECT_EQ(count, 1);
}

TEST(legacy, call3)
{
    int count = 0;
    auto callback = [&count](const std::vector<int64_t>& args) {
        count++;
        return std::accumulate(args.begin(), args.end(), 0);
    };

    auto obj = std::make_unique<connector::legacy>();
    auto result = obj->call(callback);

    EXPECT_EQ(count, 1);
    EXPECT_EQ(result, 14);
}

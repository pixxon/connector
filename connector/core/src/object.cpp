#include <connector/core/connector_core_pch.hpp>

#include <connector/core/object.hpp>

namespace connector::core {
object::object()
    : refcount_ { 0 }
{
}

object::~object()
{
}

void object::ref() const
{
    refcount_.fetch_add(1, std::memory_order_relaxed);
}

void object::unref() const
{
    if (refcount_.fetch_sub(1, std::memory_order_release) == 1) {
        std::atomic_thread_fence(std::memory_order_acquire);
        delete this;
    }
}

void intrusive_ptr_add_ref(const object* x)
{
    x->ref();
}

void intrusive_ptr_release(const object* x)
{
    x->unref();
}
}

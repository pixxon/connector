#include <connector/core/connector_core_pch.hpp>

#include <connector/core/object.hpp>
#include <connector/core/value.hpp>

namespace connector::core {
value::value(intrusive_ptr<object> obj)
    : data_ { reinterpret_cast<intptr_t>(obj.detach()) }
    , t_ { type::OBJECT }
{
}

value::value(int32_t i)
    : data_ { static_cast<intptr_t>(i) }
    , t_ { type::INT32 }
{
}

value::value(bool b)
    : data_ { static_cast<intptr_t>(b) }
    , t_ { type::BOOL }
{
}

bool value::isBool() const
{
    return t_ == type::BOOL;
}

bool value::getBool() const
{
    return static_cast<bool>(data_);
}

bool value::isObject() const
{
    return t_ == type::OBJECT;
}

intrusive_ptr<object> value::getObject() const
{
    return intrusive_ptr<object>(reinterpret_cast<object*>(data_));
}

bool value::isInt32() const
{
    return t_ == type::INT32;
}

int32_t value::getInt32() const
{
    return static_cast<int32_t>(data_);
}

value::~value()
{
    switch (t_) {
    case type::OBJECT:
        intrusive_ptr<object>(reinterpret_cast<object*>(data_), false);
        break;
    case type::INT32:
    case type::BOOL:
        break;
    }
}
}

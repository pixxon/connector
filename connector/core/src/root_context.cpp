#include <connector/core/connector_core_pch.hpp>

#include <connector/core/root_context.hpp>

namespace connector::core {
std::map<std::string, intrusive_ptr<object>>& root_context::services()
{
    static std::map<std::string, intrusive_ptr<object>> instance_;
    return instance_;
}

std::map<std::string, std::function<intrusive_ptr<object>()>>& root_context::factories()
{
    static std::map<std::string, std::function<intrusive_ptr<object>()>> instance_;
    return instance_;
}

intrusive_ptr<object> root_context::get(std::string path)
{
    if (auto it = services().find(path); it != services().end()) {
        return it->second;
    }
    return nullptr;
}

void root_context::registerObject(std::string path, intrusive_ptr<object> serv)
{
    services().emplace(path, std::move(serv));
}

void root_context::removeObject(std::string path)
{
    services().erase(path);
}

intrusive_ptr<object> root_context::create(std::string path)
{
    if (auto it = factories().find(path); it != factories().end()) {
        return it->second();
    }
    return nullptr;
}

void root_context::registerObjectFactory(std::string path, std::function<intrusive_ptr<object>()> fact)
{
    factories().emplace(path, fact);
}

void root_context::removeObjectFactory(std::string path)
{
    factories().erase(path);
}
}

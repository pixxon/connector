#include <connector/core/connector_core_pch.hpp>

#include <connector/core/legacy.hpp>

namespace connector::core {
legacy::legacy()
    : refcount_ { 0 }
{
}

legacy::~legacy()
{
}

void legacy::ref() const
{
    refcount_.fetch_add(1, std::memory_order_relaxed);
}

void legacy::unref() const
{
    if (refcount_.fetch_sub(1, std::memory_order_release) == 1) {
        std::atomic_thread_fence(std::memory_order_acquire);
        delete this;
    }
}

void legacy::call() const
{
}

void legacy::call(std::function<void()> callback) const
{
    callback();
}

int64_t legacy::call(std::function<int64_t(const std::vector<int64_t>&)> callback) const
{
    return callback({ 3, 4 }) * 2;
}

void intrusive_ptr_add_ref(const legacy* x)
{
    x->ref();
}

void intrusive_ptr_release(const legacy* x)
{
    x->unref();
}
}

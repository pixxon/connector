#include <connector/core/object.hpp>
#include <connector/core/value.hpp>

#include <gtest/gtest.h>

struct test_object
    : connector::core::object {
    connector::core::value callMethod(uint32_t id, const std::vector<connector::core::value>& args) final
    {
        int32_t result = id + args.at(0).getInt32() + args.at(1).getInt32();
        return connector::core::value { result };
    }
};

TEST(value, Int32)
{
    auto v = connector::core::value { 3 };
    EXPECT_TRUE(v.isInt32());
    EXPECT_EQ(v.getInt32(), 3);
}

TEST(value, Bool)
{
    auto v = connector::core::value { true };
    EXPECT_TRUE(v.isBool());
    EXPECT_EQ(v.getBool(), true);
}

TEST(value, Object)
{
    connector::core::intrusive_ptr<connector::core::object> obj = connector::core::make_intrusive<test_object>();
    auto v = connector::core::value { obj };
    EXPECT_TRUE(v.isObject());
    EXPECT_EQ(v.getObject(), obj);
}

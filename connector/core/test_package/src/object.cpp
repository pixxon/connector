#include <connector/core/object.hpp>

#include <numeric>

#include <gtest/gtest.h>

struct test_object
    : connector::core::object {
    connector::core::value callMethod(uint32_t id, const std::vector<connector::core::value>& args) final
    {
        int32_t result = id + args.at(0).getInt32() + args.at(1).getInt32();
        return connector::core::value { result };
    }
};

TEST(object, callMethod)
{
    test_object obj;
    EXPECT_EQ(3, obj.callMethod(0, { connector::core::value { 1 }, connector::core::value { 2 } }).getInt32());
}

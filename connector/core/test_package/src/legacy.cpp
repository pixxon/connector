#include <connector/core/legacy.hpp>

#include <numeric>

#include <gtest/gtest.h>

TEST(legacy, call1)
{
    connector::core::legacy obj;
    obj.call();
}

TEST(legacy, call2)
{
    int count = 0;
    auto callback = [&count]() {
        count++;
    };

    connector::core::legacy obj;
    obj.call(callback);

    EXPECT_EQ(count, 1);
}

TEST(legacy, call3)
{
    int count = 0;
    auto callback = [&count](const std::vector<int64_t>& args) {
        count++;
        return std::accumulate(args.begin(), args.end(), 0);
    };

    connector::core::legacy obj;
    auto result = obj.call(callback);

    EXPECT_EQ(count, 1);
    EXPECT_EQ(result, 14);
}

#include <connector/core/root_context.hpp>

#include <numeric>

#include <gtest/gtest.h>

struct test_object
    : connector::core::object {
    connector::core::value callMethod(uint32_t id, const std::vector<connector::core::value>& args) final
    {
        int32_t result = id + args.at(0).getInt32() + args.at(1).getInt32();
        return connector::core::value { result };
    }
};

TEST(root_context, noObject)
{
    EXPECT_EQ(nullptr, connector::core::root_context::get("calculator"));
}

TEST(root_context, hasObject)
{
    test_object obj;
    {
        connector::core::intrusive_ptr<connector::core::object> obj = connector::core::make_intrusive<test_object>();
        connector::core::root_context::registerObject("calculator", obj);

        EXPECT_EQ(obj, connector::core::root_context::get("calculator"));

        connector::core::root_context::removeObject("calculator");

        EXPECT_EQ(nullptr, connector::core::root_context::get("calculator"));
    }
}

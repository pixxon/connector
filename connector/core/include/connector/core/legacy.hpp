#pragma once

#include <connector/core/connector_core_pch.hpp>

#include <connector/core/intrusive_ptr.hpp>

namespace connector::core {
struct legacy {
    legacy();
    virtual ~legacy();

    void ref() const;
    void unref() const;

    virtual void call() const;
    virtual void call(std::function<void()> callback) const;
    virtual int64_t call(std::function<int64_t(const std::vector<int64_t>&)> callback) const;

private:
    mutable std::atomic<int> refcount_;
};

void intrusive_ptr_add_ref(const legacy* x);
void intrusive_ptr_release(const legacy* x);
}

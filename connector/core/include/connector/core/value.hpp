#pragma once

#include <connector/core/connector_core_pch.hpp>

#include <connector/core/intrusive_ptr.hpp>

namespace connector::core {
struct object;

struct value {
    enum class type {
        OBJECT,
        INT32,
        BOOL
    };

    value(intrusive_ptr<object>);
    value(int32_t);
    value(bool);

    ~value();

    bool isBool() const;
    bool getBool() const;

    bool isObject() const;
    intrusive_ptr<object> getObject() const;

    bool isInt32() const;
    int32_t getInt32() const;

private:
    intptr_t data_;
    type t_;
};
}

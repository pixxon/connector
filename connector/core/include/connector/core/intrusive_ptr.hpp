#pragma once

#include <connector/core/connector_core_pch.hpp>

namespace connector::core {
template <class T>
struct intrusive_ptr {
    intrusive_ptr()
        : p_ { nullptr }
    {
    }

    intrusive_ptr(T* p, bool add_ref = true)
        : p_ { p }
    {
        if (p_ != nullptr && add_ref) {
            intrusive_ptr_add_ref(p_);
        }
    }

    template <class U>
    intrusive_ptr(const intrusive_ptr<U>& rhs)
        : p_ { rhs.get() }
    {
        if (p_ != nullptr) {
            intrusive_ptr_add_ref(p_);
        }
    }

    intrusive_ptr(const intrusive_ptr& rhs)
        : p_ { rhs.p_ }
    {
        if (p_ != nullptr) {
            intrusive_ptr_add_ref(p_);
        }
    }

    intrusive_ptr(intrusive_ptr&& rhs)
        : p_(rhs.p_)
    {
        rhs.p_ = nullptr;
    }

    ~intrusive_ptr()
    {
        if (p_ != nullptr) {
            intrusive_ptr_release(p_);
        }
    }

    template <class U>
    intrusive_ptr& operator=(const intrusive_ptr<U>& rhs)
    {
        if (p_ != nullptr) {
            intrusive_ptr_release(p_);
        }
        p_ = rhs.p_;
        if (p_ != nullptr) {
            intrusive_ptr_add_ref(p_);
        }
        return *this;
    }

    intrusive_ptr& operator=(intrusive_ptr&& rhs)
    {
        if (p_ != nullptr) {
            intrusive_ptr_release(p_);
        }
        p_ = rhs.p_;
        rhs.p_ = nullptr;
        return *this;
    }

    intrusive_ptr& operator=(const intrusive_ptr& rhs)
    {
        if (p_ != nullptr) {
            intrusive_ptr_release(p_);
        }
        p_ = rhs.p_;
        if (p_ != nullptr) {
            intrusive_ptr_add_ref(p_);
        }
        return *this;
    }

    intrusive_ptr& operator=(T* rhs)
    {
        if (p_ != nullptr) {
            intrusive_ptr_release(p_);
        }
        p_ = rhs;
        return *this;
    }

    T* get() const
    {
        return p_;
    }

    T* detach()
    {
        T* ret = p_;
        p_ = nullptr;
        return ret;
    }

    T& operator*() const
    {
        return *p_;
    }

    T* operator->() const
    {
        return p_;
    }

private:
    T* p_;
};

template <class T, class U>
inline bool operator==(const intrusive_ptr<T>& a, const intrusive_ptr<U>& b)
{
    return a.get() == b.get();
}

template <class U>
inline bool operator==(std::nullptr_t, const intrusive_ptr<U>& b)
{
    return nullptr == b.get();
}

template <class T>
inline bool operator==(const intrusive_ptr<T>& a, std::nullptr_t)
{
    return a.get() == nullptr;
}

template <class T, class U>
inline bool operator!=(const intrusive_ptr<T>& a, const intrusive_ptr<U>& b)
{
    return a.get() != b.get();
}

template <class T, class U>
inline bool operator==(const intrusive_ptr<T>& a, U* b)
{
    return a.get() == b;
}

template <class T, class U>
inline bool operator!=(const intrusive_ptr<T>& a, U* b)
{
    return a.get() != b;
}

template <class T, class U>
inline bool operator==(T* a, const intrusive_ptr<U>& b)
{
    return a == b.get();
}

template <class T, class U>
inline bool operator!=(T* a, const intrusive_ptr<U>& b)
{
    return a != b.get();
}

template <class T>
inline bool operator<(intrusive_ptr<T>& a, intrusive_ptr<T>& b)
{
    return std::less<T*>()(a.get(), b.get());
}

template <class T, class U>
intrusive_ptr<T> static_pointer_cast(const intrusive_ptr<U>& p)
{
    return static_cast<T*>(p.get());
}

template <class T, class U>
intrusive_ptr<T> const_pointer_cast(const intrusive_ptr<U>& p)
{
    return const_cast<T*>(p.get());
}

template <class T, class U>
intrusive_ptr<T> dynamic_pointer_cast(const intrusive_ptr<U>& p)
{
    return dynamic_cast<T*>(p.get());
}

template <class T, class U>
intrusive_ptr<T> reinterpret_pointer_cast(const intrusive_ptr<U>& p)
{
    return reinterpret_cast<T*>(p.get());
}

template <class T, bool AddRef = true, class... Args>
intrusive_ptr<T> make_intrusive(Args&&... args)
{
    return { new T { std::forward<Args>(args)... }, AddRef };
}

template <class T, bool AddRef = true>
intrusive_ptr<T> make_intrusive(T* p)
{
    return { p, AddRef };
}
}

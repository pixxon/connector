#pragma once

#include <connector/core/connector_core_pch.hpp>

#include <connector/core/intrusive_ptr.hpp>
#include <connector/core/object.hpp>

namespace connector::core {
struct root_context {
    static intrusive_ptr<object> get(std::string path);
    static void registerObject(std::string path, intrusive_ptr<object>);
    static void removeObject(std::string path);

    static intrusive_ptr<object> create(std::string path);
    static void registerObjectFactory(std::string path, std::function<intrusive_ptr<object>()>);
    static void removeObjectFactory(std::string path);

private:
    static std::map<std::string, intrusive_ptr<object>>& services();
    static std::map<std::string, std::function<intrusive_ptr<object>()>>& factories();
};
}

#pragma once

#include <connector/core/connector_core_pch.hpp>

#include <connector/core/intrusive_ptr.hpp>
#include <connector/core/value.hpp>

namespace connector::core {
struct object {
    object();
    virtual ~object();

    virtual void ref() const;
    virtual void unref() const;

    virtual value callMethod(uint32_t id, const std::vector<value>& args) = 0;

private:
    mutable std::atomic<int> refcount_;
};

void intrusive_ptr_add_ref(const object* x);
void intrusive_ptr_release(const object* x);
}

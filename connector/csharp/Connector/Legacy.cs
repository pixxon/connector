using System.Collections;
using System.Runtime.InteropServices;

namespace Connector;

public class Legacy
{
    private readonly IntPtr handle;

    public Legacy()
    {
        handle = Native.LegacyFunctions.Create();
    }

    public Legacy(IntPtr handle, bool addRef = true)
    {
        this.handle = handle;

        if (addRef)
        {
            Native.LegacyFunctions.Ref(handle);
        }
    }

    ~Legacy()
    {
        Native.LegacyFunctions.Unref(handle);
    }

    virtual public void Call()
    {
        Native.LegacyFunctions.Call(handle);
    }

    virtual public void Call(Action action)
    {
        GCHandle callbackHandle = GCHandle.Alloc(action);

        var data = new Native.LegacyTypes.Call2CallbackType
        {
            userdata = GCHandle.ToIntPtr(callbackHandle),
            userdata_destructor = &Call2Destructor,
            callback = &Call2Callback
        };

        Native.LegacyFunctions.Call(handle, data);
    }

    virtual unsafe public Int64 Call(Func<List<Int64>, Int64> func)
    {
        GCHandle callbackHandle = GCHandle.Alloc(func);

        var data = new Native.LegacyTypes.Call3CallbackType
        {
            userdata = GCHandle.ToIntPtr(callbackHandle),
            userdata_destructor = &Call3Destructor,
            callback = &Call3Callback
        };

        return Native.LegacyFunctions.Call(handle, data);
    }

    [UnmanagedCallersOnly]
    private static void Call2Callback(IntPtr callback)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(callback);
        if (callbackHandle.Target is Action action)
        {
            action.Invoke();
        }
    }

    [UnmanagedCallersOnly]
    private static void Call2Destructor(IntPtr userdata)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(userdata);
        callbackHandle.Free();
    }

    [UnmanagedCallersOnly]
    private unsafe static Int64 Call3Callback(IntPtr userdata, UInt64 size, Int64* buffer)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(userdata);
        if (callbackHandle.Target is Func<List<Int64>, Int64> action)
        {
            var l = new List<Int64>();
            for (UInt64 i = 0; i < size; ++i)
            {
                l.Add(buffer[i]);
            }
            return action.Invoke(l);
        }
        return 0;
    }

    [UnmanagedCallersOnly]
    private static void Call3Destructor(IntPtr callback)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(callback);
        callbackHandle.Free();
    }
}

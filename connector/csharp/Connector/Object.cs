namespace Connector;

public class Object
{
    internal readonly IntPtr handle;

    public Object(IntPtr handle, bool addRef = true)
    {
        this.handle = handle;

        if (addRef)
        {
            Native.ObjectFunctions.Ref(handle);
        }
    }

    ~Object()
    {
        Native.ObjectFunctions.Unref(handle);
    }

    virtual unsafe public Value CallMethod(UInt32 id, List<Value> args)
    {
        Native.ValueTypes.Value[] parameters = args.Select(e => e.ToValue()).ToArray();
        fixed (Native.ValueTypes.Value* first = &parameters[0])
        {
            return new Value(Native.ObjectFunctions.CallMethod(handle, id, parameters.Length, first));
        }
    }
    public override bool Equals(object? obj) => Equals(obj as Object);

    public bool Equals(Object? v)
    {
        if (v is null)
        {
            return false;
        }

        if (ReferenceEquals(this, v))
        {
            return true;
        }

        if (GetType() != v.GetType())
        {
            return false;
        }

        return handle == v.handle;
    }
    public override int GetHashCode() => handle.GetHashCode();

    public static bool operator ==(Object? lhs, Object? rhs)
    {
        if (lhs is null)
        {
            if (rhs is null)
            {
                return true;
            }

            return false;
        }

        return lhs.Equals(rhs);
    }

    public static bool operator !=(Object? lhs, Object? rhs) => !(lhs == rhs);
}

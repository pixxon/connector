using System.Collections;
using System.Collections.Specialized;
using System.Runtime.InteropServices;

namespace Connector;

public class Value
{
    internal Value(Native.ValueTypes.Value value)
    {
        if (Native.ValueFunctions.IsBool(value))
        {
            type = Type.Bool;
            b = Native.ValueFunctions.GetBool(value);
        }
        else if (Native.ValueFunctions.IsInt32(value))
        {
            type = Type.Int32;
            i = Native.ValueFunctions.GetInt32(value);
        }
        else if (Native.ValueFunctions.IsObject(value))
        {
            type = Type.Object;
            o = new Object(Native.ValueFunctions.GetObject(value));
        }
        else
        {
            throw new Exception();
        }
    }

    internal Native.ValueTypes.Value ToValue()
    {
        return type switch
        {
            Type.Bool => Native.ValueFunctions.Create(b!.Value),
            Type.Int32 => Native.ValueFunctions.Create(i!.Value),
            Type.Object => Native.ValueFunctions.Create(o!.handle),
            _ => throw new Exception(),
        };
    }

    private enum Type
    {
        Bool,
        Int32,
        Object
    }

    private readonly Type type;

    private readonly bool? b;
    private readonly Int32? i;
    private readonly Object? o;

    public Value(bool b)
    {
        type = Type.Bool;
        this.b = b;
    }

    public Value(Int32 i)
    {
        type = Type.Int32;
        this.i = i;
    }

    public Value(Object o)
    {
        type = Type.Object;
        this.o = o;
    }

    public bool IsBool()
    {
        return type == Type.Bool;
    }

    public bool GetBool()
    {
        return b!.Value;
    }

    public bool IsInt32()
    {
        return type == Type.Int32;
    }

    public Int32 GetInt32()
    {
        return i!.Value;
    }

    public bool IsObject()
    {
        return type == Type.Object;
    }

    public Object GetObject()
    {
        return o!;
    }

    public override bool Equals(object? obj) => Equals(obj as Value);

    public bool Equals(Value? v)
    {
        if (v is null)
        {
            return false;
        }

        if (ReferenceEquals(this, v))
        {
            return true;
        }

        if (GetType() != v.GetType())
        {
            return false;
        }

        return (type == v.type) && (i == v.i) && (b == v.b) && (o == v.o);
    }
    public override int GetHashCode() => (type, i, b, o).GetHashCode();

    public static bool operator ==(Value? lhs, Value? rhs)
    {
        if (lhs is null)
        {
            if (rhs is null)
            {
                return true;
            }

            return false;
        }

        return lhs.Equals(rhs);
    }

    public static bool operator !=(Value? lhs, Value? rhs) => !(lhs == rhs);

    public override string? ToString()
    {
        return type switch
        {
            Type.Bool => b!.ToString(),
            Type.Int32 => i!.ToString(),
            Type.Object => o!.ToString(),
            _ => throw new Exception(),
        };
    }
}

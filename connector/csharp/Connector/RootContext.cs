using System.Runtime.InteropServices;

namespace Connector;

static public class RootContext
{
    public unsafe static Object Get(string name)
    {
        fixed (char* ptr = name)
        {
            return new Object(Native.RootContextFunctions.Get(ptr));
        }
    }

    public unsafe static void RegisterObject(string name, Object service)
    {
        fixed (char* ptr = name)
        {
            Native.RootContextFunctions.RegisterObject(ptr, service.handle);
        }
    }

    public unsafe static void RemoveObject(string name)
    {
        fixed (char* ptr = name)
        {
            Native.RootContextFunctions.RemoveObject(ptr);
        }
    }

    public unsafe static Object Create(string name)
    {
        fixed (char* ptr = name)
        {
            return new Object(Native.RootContextFunctions.Create(ptr));
        }
    }

    public unsafe static void RegisterObjectFactory(string name, Func<Object> factory)
    {
        var factoryHandle = GCHandle.Alloc(factory);
        var data = new Native.RootContextTypes.RegisterObjectFactoryCallbackType
        {
            userdata = GCHandle.ToIntPtr(factoryHandle),
            userdata_destructor = &RegisterObjectFactoryDestructor,
            callback = &RegisterObjectFactoryCallback
        };

        fixed (char* ptr = name)
        {
            Native.RootContextFunctions.RegisterObjectFactory(ptr, data);
        }
    }

    public unsafe static void RemoveObjectFactory(string name)
    {
        fixed (char* ptr = name)
        {
            Native.RootContextFunctions.RemoveObjectFactory(ptr);
        }
    }


    [UnmanagedCallersOnly]
    private static IntPtr RegisterObjectFactoryCallback(IntPtr callback)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(callback);
        if (callbackHandle.Target is Func<Object> action)
        {
            return action.Invoke().handle;
        }
        throw new Exception();
    }

    [UnmanagedCallersOnly]
    private static void RegisterObjectFactoryDestructor(IntPtr callback)
    {
        GCHandle callbackHandle = GCHandle.FromIntPtr(callback);
        callbackHandle.Free();
    }
}

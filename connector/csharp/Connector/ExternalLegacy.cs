using System.Runtime.InteropServices;

namespace Connector;

public class ExternalLegacy(ExternalLegacyImpl impl) : Legacy(Native.ExternalLegacyFunctions.Create(GCHandle.ToIntPtr(GCHandle.Alloc(impl)), table), false)
{
    private readonly ExternalLegacyImpl impl = impl;

    private static Native.ExternalLegacyTypes.FuncTable table = new()
    {
        destroy = &Destroy,
        call1 = &Call,
        call2 = &Call,
        call3 = &Call
    };

    override public void Call()
    {
        impl.Call();
    }

    override public void Call(Action action)
    {
        impl.Call(action);
    }

    override public Int64 Call(Func<List<Int64>, Int64> func)
    {
        return impl.Call(func);
    }

    [UnmanagedCallersOnly]
    private static void Destroy(IntPtr self)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        handle.Free();
    }

    [UnmanagedCallersOnly]
    private static void Call(IntPtr self)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        if (handle.Target is ExternalLegacyImpl obj)
        {
            obj.Call();
        }
    }

    [UnmanagedCallersOnly]
    private static void Call(IntPtr self, Native.LegacyTypes.Call2CallbackType callback)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        if (handle.Target is ExternalLegacyImpl obj)
        {
            unsafe void cb()
            {
                callback.callback(callback.userdata);
                if (callback.userdata_destructor != null)
                {
                    callback.userdata_destructor(callback.userdata);
                }
            };
            obj.Call(cb);
        }
    }

    [UnmanagedCallersOnly]
    private static Int64 Call(IntPtr self, Native.LegacyTypes.Call3CallbackType callback)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        if (handle.Target is ExternalLegacyImpl obj)
        {
            unsafe Int64 cb(List<Int64> l)
            {
                Int64 result = 0;

                Int64[] buffer = [.. l];
                fixed (Int64* first = &buffer[0])
                {
                    result = callback.callback(callback.userdata, (UInt64)buffer.Length, first);
                }
                if (callback.userdata_destructor != null)
                {
                    callback.userdata_destructor(callback.userdata);
                }

                return result;
            };
            return obj.Call(cb);
        }
        throw new Exception();
    }
}

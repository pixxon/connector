namespace Connector;

public abstract class ExternalObjectImpl
{
    public abstract Value CallMethod(UInt32 id, List<Value> args);
}

namespace Connector;

public class ExternalLegacyImpl
{
#pragma warning disable CA1822 // Mark members as static
    public void Call()
#pragma warning restore CA1822 // Mark members as static
    {
        return;
    }

#pragma warning disable CA1822 // Mark members as static
    public void Call(Action action)
#pragma warning restore CA1822 // Mark members as static
    {
        action.Invoke();
    }

#pragma warning disable CA1822 // Mark members as static
    public Int64 Call(Func<List<Int64>, Int64> func)
#pragma warning restore CA1822 // Mark members as static
    {
        return func.Invoke([3, 4]) * 2;
    }
}

using System.Runtime.InteropServices;

namespace Connector;

public class ExternalObject(ExternalObjectImpl impl) : Object(Native.ExternalObjectFunctions.Create(GCHandle.ToIntPtr(GCHandle.Alloc(impl)), table), false)
{
    private readonly ExternalObjectImpl impl = impl;

    private unsafe static Native.ExternalObjectTypes.FuncTable table = new()
    {
        destroy = &Destroy,
        callMethod = &CallMethod,
    };

    override public Value CallMethod(UInt32 id, List<Value> args)
    {
        return impl.CallMethod(id, args);
    }

    [UnmanagedCallersOnly]
    private static void Destroy(IntPtr self)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        handle.Free();
    }

    [UnmanagedCallersOnly]
    private unsafe static Native.ValueTypes.Value CallMethod(IntPtr self, UInt32 id, Int64 size, Native.ValueTypes.Value* args)
    {
        GCHandle handle = GCHandle.FromIntPtr(self);
        if (handle.Target is ExternalObjectImpl obj)
        {
            var l = new List<Value>();
            for (Int64 i = 0; i < size; ++i)
            {
                l.Add(new Value(args[i]));
            }

            var r = obj.CallMethod(id, l);

            return r.ToValue();
        }
        throw new Exception();
    }
}

using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class ValueFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_createBool__")]
    internal static partial ValueTypes.Value Create([MarshalAs(UnmanagedType.Bool)] bool b);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_createObject__")]
    internal static partial ValueTypes.Value Create(IntPtr obj);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_createInt32__")]
    internal static partial ValueTypes.Value Create(Int32 i);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_destroy__")]
    internal static partial void Destroy(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_isBool")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static partial bool IsBool(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_getBool")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static partial bool GetBool(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_isObject")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static partial bool IsObject(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_getObject")]
    internal static partial IntPtr GetObject(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_isInt32")]
    [return: MarshalAs(UnmanagedType.Bool)]
    internal static partial bool IsInt32(ValueTypes.Value handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_value_getInt32")]
    internal static partial Int32 GetInt32(ValueTypes.Value handle);
}

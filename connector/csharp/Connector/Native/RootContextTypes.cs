using System.Runtime.InteropServices;

namespace Connector.Native;

internal class RootContextTypes
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct RegisterObjectFactoryCallbackType
    {
        public IntPtr userdata;
        public delegate* unmanaged<IntPtr, void> userdata_destructor;
        public delegate* unmanaged<IntPtr, IntPtr> callback;
    }
}

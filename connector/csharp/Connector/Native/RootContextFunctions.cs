using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class RootContextFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_get")]
    internal unsafe static partial IntPtr Get(char* path);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_registerObject")]
    internal unsafe static partial void RegisterObject(char* path, IntPtr service);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_removeObject")]
    internal unsafe static partial void RemoveObject(char* path);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_create")]
    internal unsafe static partial IntPtr Create(char* path);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_registerObjectFactory")]
    internal unsafe static partial void RegisterObjectFactory(char* path, RootContextTypes.RegisterObjectFactoryCallbackType factory);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_root_context_removeObjectFactory")]
    internal unsafe static partial void RemoveObjectFactory(char* path);
}

using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class ExternalLegacyFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_external_legacy_create__")]
    internal static partial IntPtr Create(IntPtr self, ExternalLegacyTypes.FuncTable table);
}

using System.Runtime.InteropServices;

namespace Connector.Native;

static internal class ExternalLegacyTypes
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct FuncTable
    {
        public delegate* unmanaged<IntPtr, void> destroy;
        public delegate* unmanaged<IntPtr, void> call1;
        public delegate* unmanaged<IntPtr, LegacyTypes.Call2CallbackType, void> call2;
        public delegate* unmanaged<IntPtr, LegacyTypes.Call3CallbackType, Int64> call3;
    }
}

using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class ObjectFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_object_ref__")]
    internal static partial void Ref(IntPtr handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_object_unref__")]
    internal static partial void Unref(IntPtr handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_object_callMethod")]
    internal unsafe static partial ValueTypes.Value CallMethod(IntPtr handle, UInt32 name, Int64 argc, ValueTypes.Value* argv);
}

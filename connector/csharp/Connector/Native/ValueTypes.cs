using System.Runtime.InteropServices;

namespace Connector.Native;

static internal class ValueTypes
{
    internal enum ValueType
    {
        OBJECT = 0,
        INT32 = 1,
        BOOL = 2
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct Value
    {
        public ValueType type;
        public IntPtr data;
    }
}

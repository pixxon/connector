using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class LegacyFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_create__")]
    internal static partial IntPtr Create();

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_ref__")]
    internal static partial void Ref(IntPtr handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_unref__")]
    internal static partial void Unref(IntPtr handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_call1")]
    internal static partial void Call(IntPtr handle);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_call2")]
    internal static partial void Call(IntPtr handle, LegacyTypes.Call2CallbackType callback);

    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_legacy_call3")]
    internal static partial Int64 Call(IntPtr handle, LegacyTypes.Call3CallbackType callback);
}

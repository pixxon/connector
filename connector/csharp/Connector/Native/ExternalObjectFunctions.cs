using System.Runtime.InteropServices;

namespace Connector.Native;

static internal partial class ExternalObjectFunctions
{
    [LibraryImport("libconnector-0.0.1.so", EntryPoint = "connector_external_object_create__")]
    internal static partial IntPtr Create(IntPtr self, ExternalObjectTypes.FuncTable table);
}

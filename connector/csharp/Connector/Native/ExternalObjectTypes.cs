using System.Runtime.InteropServices;

namespace Connector.Native;

static internal class ExternalObjectTypes
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct FuncTable
    {
        public delegate* unmanaged<IntPtr, void> destroy;
        public delegate* unmanaged<IntPtr, UInt32, Int64, ValueTypes.Value*, ValueTypes.Value> callMethod;
    }
}

using System.Runtime.InteropServices;

namespace Connector.Native;

internal class LegacyTypes
{
    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct Call2CallbackType
    {
        public IntPtr userdata;
        public delegate* unmanaged<IntPtr, void> userdata_destructor;
        public delegate* unmanaged<IntPtr, void> callback;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct Call3CallbackType
    {
        public IntPtr userdata;
        public delegate* unmanaged<IntPtr, void> userdata_destructor;
        public delegate* unmanaged<IntPtr, UInt64, Int64*, Int64> callback;
    }
}

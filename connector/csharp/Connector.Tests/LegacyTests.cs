using Xunit;

namespace Connector.Tests;

public class LegacyTests
{
    [Fact]
    public void TestCall1()
    {
        var l = new Legacy();
        l.Call();
    }

    [Fact]
    public void TestCall2()
    {
        var l = new Legacy();
        var count = 0;

        l.Call(() => count++);

        Assert.Equal(1, count);
    }

    [Fact]
    public void TestCall3()
    {
        var l = new Legacy();
        var count = 0;

        var result = l.Call((l) =>
        {
            count++;
            return l.Sum();
        });

        Assert.Equal(1, count);
        Assert.Equal(14, result);
    }
}

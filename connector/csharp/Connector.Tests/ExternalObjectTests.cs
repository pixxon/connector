using Xunit;

namespace Connector.Tests;

public class ExternalObjectTests
{
    class TestImpl : ExternalObjectImpl
    {
        public override Value CallMethod(uint id, List<Value> args)
        {
            Int32 result = (Int32)(id + args.ElementAt(0).GetInt32() + args.ElementAt(1).GetInt32());
            return new Value(result);
        }
    }

    [Fact]
    public void TestCallMethod()
    {
        var obj = new ExternalObject(new TestImpl());
        var result = obj.CallMethod(0, [new Value(1), new Value(2)]);
        Assert.Equal(new Value(3), result);
    }
}

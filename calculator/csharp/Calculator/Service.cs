using Connector;

namespace Calculator;

public class Service : ExternalObjectImpl
{
    private const UInt32 CALCULATOR_ADD1_ID = 0;
    private const UInt32 CALCULATOR_SUB1_ID = 1;
    private const UInt32 CALCULATOR_MUL1_ID = 2;
    private const UInt32 CALCULATOR_DIV1_ID = 3;
    private const UInt32 CALCULATOR_ADD2_ID = 4;

    public override Value CallMethod(UInt32 id, List<Value> args)
    {
        return id switch
        {
            CALCULATOR_ADD1_ID => new Value(Add(args.ElementAt(0).GetInt32(), args.ElementAt(1).GetInt32())),
            CALCULATOR_SUB1_ID => new Value(Sub(args.ElementAt(0).GetInt32(), args.ElementAt(1).GetInt32())),
            CALCULATOR_MUL1_ID => new Value(Mul(args.ElementAt(0).GetInt32(), args.ElementAt(1).GetInt32())),
            CALCULATOR_DIV1_ID => new Value(Div(args.ElementAt(0).GetInt32(), args.ElementAt(1).GetInt32())),
            CALCULATOR_ADD2_ID => new Value(Add(args.ElementAt(0).GetInt32(), args.ElementAt(1).GetInt32(), args.ElementAt(2).GetInt32())),
            _ => throw new Exception(),
        };
    }

    public Int32 Add(Int32 a, Int32 b)
    {
        return a + b;
    }

    public Int32 Sub(Int32 a, Int32 b)
    {
        return a - b;
    }

    public Int32 Mul(Int32 a, Int32 b)
    {
        return a * b;
    }

    public Int32 Div(Int32 a, Int32 b)
    {
        return a / b;
    }
    public Int32 Add(Int32 a, Int32 b, Int32 c)
    {
        return a + b + c;
    }
}
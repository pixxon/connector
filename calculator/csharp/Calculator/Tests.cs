using Xunit;
using Connector;

namespace Calculator;

public abstract class Tests
{
    [Fact]
    public void TestAdd()
    {
        var obj = RootContext.Get("calculator");
        var calc = new Client(obj);
        Assert.Equal(3, calc.Add(1, 2));
    }

    [Fact]
    public void TestSub()
    {
        var obj = RootContext.Get("calculator");
        var calc = new Client(obj);
        Assert.Equal(1, calc.Sub(2, 1));
    }

    [Fact]
    public void TestMul()
    {
        var obj = RootContext.Get("calculator");
        var calc = new Client(obj);
        Assert.Equal(8, calc.Mul(4, 2));
    }
    [Fact]
    public void TestDiv()
    {
        var obj = RootContext.Get("calculator");
        var calc = new Client(obj);
        Assert.Equal(2, calc.Div(4, 2));
    }
    [Fact]
    public void TestAdd2()
    {
        var obj = RootContext.Get("calculator");
        var calc = new Client(obj);
        Assert.Equal(6, calc.Add(1, 2, 3));
    }
}

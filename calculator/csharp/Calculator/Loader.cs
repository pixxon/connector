using Connector;

namespace Calculator;

public static class Loader
{
    public static void Load()
    {
        RootContext.RegisterObject("calculator", new ExternalObject(new Service()));
    }

    public static void Unload()
    {
        RootContext.RemoveObject("calculator");
    }
}

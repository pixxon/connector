using Connector;

namespace Calculator;

class Client(Connector.Object obj)
{
    private static readonly UInt32 CALCULATOR_ADD1_ID = 0;
    private static readonly UInt32 CALCULATOR_SUB1_ID = 1;
    private static readonly UInt32 CALCULATOR_MUL1_ID = 2;
    private static readonly UInt32 CALCULATOR_DIV1_ID = 3;
    private static readonly UInt32 CALCULATOR_ADD2_ID = 4;

    private readonly Connector.Object obj = obj;

    public Int32 Add(Int32 a, Int32 b)
    {
        return obj.CallMethod(CALCULATOR_ADD1_ID, [new Value(a), new Value(b)]).GetInt32();
    }

    public Int32 Sub(Int32 a, Int32 b)
    {
        return obj.CallMethod(CALCULATOR_SUB1_ID, [new Value(a), new Value(b)]).GetInt32();
    }

    public Int32 Mul(Int32 a, Int32 b)
    {
        return obj.CallMethod(CALCULATOR_MUL1_ID, [new Value(a), new Value(b)]).GetInt32();
    }

    public Int32 Div(Int32 a, Int32 b)
    {
        return obj.CallMethod(CALCULATOR_DIV1_ID, [new Value(a), new Value(b)]).GetInt32();
    }

    public Int32 Add(Int32 a, Int32 b, Int32 c)
    {
        return obj.CallMethod(CALCULATOR_ADD2_ID, [new Value(a), new Value(b), new Value(c)]).GetInt32();
    }
}

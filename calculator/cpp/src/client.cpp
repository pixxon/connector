#include <calculator/client.hpp>

namespace calculator {
client::client(std::shared_ptr<connector::object> obj)
    : obj_ { std::move(obj) }
{
}

int32_t client::add(int32_t a, int32_t b)
{
    return obj_->callMethod(CALCULATOR_ADD1_ID, { connector::value { a }, connector::value { b } }).getInt32();
}

int32_t client::sub(int32_t a, int32_t b)
{
    return obj_->callMethod(CALCULATOR_SUB1_ID, { connector::value { a }, connector::value { b } }).getInt32();
}

int32_t client::mul(int32_t a, int32_t b)
{
    return obj_->callMethod(CALCULATOR_MUL1_ID, { connector::value { a }, connector::value { b } }).getInt32();
}

int32_t client::div(int32_t a, int32_t b)
{
    return obj_->callMethod(CALCULATOR_DIV1_ID, { connector::value { a }, connector::value { b } }).getInt32();
}

int32_t client::add(int32_t a, int32_t b, int32_t c)
{
    return obj_->callMethod(CALCULATOR_ADD2_ID, { connector::value { a }, connector::value { b }, connector::value { c } }).getInt32();
}

}
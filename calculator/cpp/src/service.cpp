#include <calculator/service.hpp>

namespace calculator {
connector::value service::callMethod(uint32_t id, const std::vector<connector::value>& args)
{
    switch (id) {
    case CALCULATOR_ADD1_ID:
        return connector::value { add(args.at(0).getInt32(), args.at(1).getInt32()) };
    case CALCULATOR_SUB1_ID:
        return connector::value { sub(args.at(0).getInt32(), args.at(1).getInt32()) };
    case CALCULATOR_MUL1_ID:
        return connector::value { mul(args.at(0).getInt32(), args.at(1).getInt32()) };
    case CALCULATOR_DIV1_ID:
        return connector::value { div(args.at(0).getInt32(), args.at(1).getInt32()) };
    case CALCULATOR_ADD2_ID:
        return connector::value { add(args.at(0).getInt32(), args.at(1).getInt32(), args.at(2).getInt32()) };
    default:
        throw std::exception();
    }
}

int32_t service::add(int32_t a, int32_t b)
{
    return a + b;
}

int32_t service::sub(int32_t a, int32_t b)
{
    return a - b;
}

int32_t service::mul(int32_t a, int32_t b)
{
    return a * b;
}

int32_t service::div(int32_t a, int32_t b)
{
    return a / b;
}

int32_t service::add(int32_t a, int32_t b, int32_t c)
{
    return a + b + c;
}
}

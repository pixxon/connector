#include <calculator/loader.hpp>

#include <calculator/service.hpp>

#include <connector/external_object.hpp>
#include <connector/root_context.hpp>

#include <string>

namespace calculator {
void loader::load()
{
    connector::root_context::registerObject("calculator", std::make_shared<connector::external_object>(new service {}));
}

void loader::unload()
{
    connector::root_context::removeObject("calculator");
}
}
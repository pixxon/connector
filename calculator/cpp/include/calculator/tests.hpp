#pragma once

#include <calculator/client.hpp>

#include <connector/root_context.hpp>

#include <gtest/gtest.h>

void test_calculator_add1()
{
    auto calc = calculator::client { connector::root_context::get("calculator") };
    EXPECT_EQ(calc.add(1, 2), 3);
}

void test_calculator_sub1()
{
    auto calc = calculator::client { connector::root_context::get("calculator") };
    EXPECT_EQ(calc.sub(2, 1), 1);
}

void test_calculator_mul1()
{
    auto calc = calculator::client { connector::root_context::get("calculator") };
    EXPECT_EQ(calc.mul(4, 2), 8);
}

void test_calculator_div1()
{
    auto calc = calculator::client { connector::root_context::get("calculator") };
    EXPECT_EQ(calc.div(4, 2), 2);
}

void test_calculator_add2()
{
    auto calc = calculator::client { connector::root_context::get("calculator") };
    EXPECT_EQ(calc.add(1, 2, 3), 6);
}

#pragma once

#include <connector/object.hpp>

namespace calculator {
struct client {
    client(std::shared_ptr<connector::object> obj);

    int32_t add(int32_t a, int32_t b);
    int32_t sub(int32_t a, int32_t b);
    int32_t mul(int32_t a, int32_t b);
    int32_t div(int32_t a, int32_t b);
    int32_t add(int32_t a, int32_t b, int32_t c);

private:
    static constexpr uint32_t CALCULATOR_ADD1_ID = 0l;
    static constexpr uint32_t CALCULATOR_SUB1_ID = 1l;
    static constexpr uint32_t CALCULATOR_MUL1_ID = 2l;
    static constexpr uint32_t CALCULATOR_DIV1_ID = 3l;
    static constexpr uint32_t CALCULATOR_ADD2_ID = 4l;

    std::shared_ptr<connector::object> obj_;
};

}
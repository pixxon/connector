from .service import Service

from connector import ExternalObject, RootContext

class Loader:
	@staticmethod
	def load():
		RootContext.registerObject("calculator", ExternalObject(Service()))

	@staticmethod
	def unload():
		RootContext.removeObject("calculator")

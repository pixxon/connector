import unittest

from .client import Client
from connector import RootContext

class Tests(unittest.TestCase):
	def test_add1(self):
		obj = RootContext.get("calculator")
		calc = Client(obj)
		self.assertEqual( calc.add1(1, 2), 3 )
	
	def test_sub1(self):
		obj = RootContext.get("calculator")
		calc = Client(obj)
		self.assertEqual( calc.sub1(2, 1), 1 )

	def test_mul1(self):
		obj = RootContext.get("calculator")
		calc = Client(obj)
		self.assertEqual( calc.mul1(4, 2), 8 )

	def test_div1(self):
		obj = RootContext.get("calculator")
		calc = Client(obj)
		self.assertEqual( calc.sub1(4, 2), 2 )

	def test_add2(self):
		obj = RootContext.get("calculator")
		calc = Client(obj)
		self.assertEqual( calc.add2(1, 2, 3), 6 )

from connector import ExternalObjectImpl, Value

class Service(ExternalObjectImpl):
	CALCULATOR_SAMPLE_ADD1_ID = 0
	CALCULATOR_SAMPLE_SUB1_ID = 1
	CALCULATOR_SAMPLE_MUL1_ID = 2
	CALCULATOR_SAMPLE_DIV1_ID = 3
	CALCULATOR_SAMPLE_ADD2_ID = 4

	def callMethod(self, id, args):
		if id == self.CALCULATOR_SAMPLE_ADD1_ID:
			return Value( self.add1(args[0].getInt32(), args[1].getInt32()) )
		elif id == self.CALCULATOR_SAMPLE_SUB1_ID:
			return Value( self.sub1(args[0].getInt32(), args[1].getInt32()) )
		elif id == self.CALCULATOR_SAMPLE_MUL1_ID:
			return Value( self.mul1(args[0].getInt32(), args[1].getInt32()) )
		elif id == self.CALCULATOR_SAMPLE_DIV1_ID:
			return Value( self.div1(args[0].getInt32(), args[1].getInt32()) )
		elif id == self.CALCULATOR_SAMPLE_ADD2_ID:
			return Value( self.add2(args[0].getInt32(), args[1].getInt32(), args[2].getInt32()) )
		else:
			raise Exception()

	def add1(self, a, b):
		return a + b

	def sub1(self, a, b):
		return a - b

	def mul1(self, a, b):
		return a * b

	def div1(self, a, b):
		return int(a / b)
	
	def add2(self, a, b, c):
		return a + b + c

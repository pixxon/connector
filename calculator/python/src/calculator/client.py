from connector import Object, Value

class Client:
	CALCULATOR_ADD1_ID = 0
	CALCULATOR_SUB1_ID = 1
	CALCULATOR_MUL1_ID = 2
	CALCULATOR_DIV1_ID = 3
	CALCULATOR_ADD2_ID = 4

	def __init__(self, obj):
		self.__obj = obj

	def add1(self, a, b):
		return self.__obj.callMethod(self.CALCULATOR_ADD1_ID, [Value(a), Value(b)]).getInt32()

	def sub1(self, a, b):
		return self.__obj.callMethod(self.CALCULATOR_SUB1_ID, [Value(a), Value(b)]).getInt32()

	def mul1(self, a, b):
		return self.__obj.callMethod(self.CALCULATOR_MUL1_ID, [Value(a), Value(b)]).getInt32()

	def div1(self, a, b):
		return self.__obj.callMethod(self.CALCULATOR_DIV1_ID, [Value(a), Value(b)]).getInt32()

	def add2(self, a, b, c):
		return self.__obj.callMethod(self.CALCULATOR_ADD2_ID, [Value(a), Value(b), Value(c)]).getInt32()

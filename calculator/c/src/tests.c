#include <calculator/tests.h>

#include <calculator/client.h>

#include <connector/root_context.h>

START_TEST(test_calculator_add1)
{
    connector_object_handle object = connector_root_context_get("calculator");
    ck_assert_int_eq(3, calculator_client_add1(object, 1, 2));
    connector_object_unref__(object);
}
END_TEST

START_TEST(test_calculator_sub1)
{
    connector_object_handle object = connector_root_context_get("calculator");
    ck_assert_int_eq(1, calculator_client_sub1(object, 2, 1));
    connector_object_unref__(object);
}
END_TEST

START_TEST(test_calculator_mul1)
{
    connector_object_handle object = connector_root_context_get("calculator");
    ck_assert_int_eq(8, calculator_client_mul1(object, 4, 2));
    connector_object_unref__(object);
}
END_TEST

START_TEST(test_calculator_div1)
{
    connector_object_handle object = connector_root_context_get("calculator");
    ck_assert_int_eq(2, calculator_client_div1(object, 4, 2));
    connector_object_unref__(object);
}
END_TEST

START_TEST(test_calculator_add2)
{
    connector_object_handle object = connector_root_context_get("calculator");
    ck_assert_int_eq(6, calculator_client_add2(object, 1, 2, 3));
    connector_object_unref__(object);
}
END_TEST

Suite* test_calculator_suite(void (*setup)(), void (*teardown)())
{
    TCase* tc_test_add1 = tcase_create("test_add1");
    tcase_add_checked_fixture(tc_test_add1, setup, teardown);
    tcase_add_test(tc_test_add1, test_calculator_add1);

    TCase* tc_test_sub1 = tcase_create("test_sub1");
    tcase_add_checked_fixture(tc_test_sub1, setup, teardown);
    tcase_add_test(tc_test_sub1, test_calculator_sub1);

    TCase* tc_test_mul1 = tcase_create("test_mul1");
    tcase_add_checked_fixture(tc_test_mul1, setup, teardown);
    tcase_add_test(tc_test_mul1, test_calculator_mul1);

    TCase* tc_test_div1 = tcase_create("test_div1");
    tcase_add_checked_fixture(tc_test_div1, setup, teardown);
    tcase_add_test(tc_test_div1, test_calculator_div1);

    TCase* tc_test_add2 = tcase_create("test_add2");
    tcase_add_checked_fixture(tc_test_add2, setup, teardown);
    tcase_add_test(tc_test_add2, test_calculator_add2);

    Suite* ts_calculator = suite_create("calculator");
    suite_add_tcase(ts_calculator, tc_test_add1);
    suite_add_tcase(ts_calculator, tc_test_sub1);
    suite_add_tcase(ts_calculator, tc_test_mul1);
    suite_add_tcase(ts_calculator, tc_test_div1);
    suite_add_tcase(ts_calculator, tc_test_add2);

    return ts_calculator;
}

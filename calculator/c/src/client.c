#include <calculator/client.h>

#define CALCULATOR_ADD1_ID 0
#define CALCULATOR_SUB1_ID 1
#define CALCULATOR_MUL1_ID 2
#define CALCULATOR_DIV1_ID 3
#define CALCULATOR_ADD2_ID 4

int32_t calculator_client_add1(connector_object_handle obj, int32_t a, int32_t b)
{
    connector_value args[2] = { connector_value_createInt32__(a), connector_value_createInt32__(b) };
    return connector_value_getInt32(connector_object_callMethod(obj, CALCULATOR_ADD1_ID, 2, args));
}

int32_t calculator_client_sub1(connector_object_handle obj, int32_t a, int32_t b)
{
    connector_value args[2] = { connector_value_createInt32__(a), connector_value_createInt32__(b) };
    return connector_value_getInt32(connector_object_callMethod(obj, CALCULATOR_SUB1_ID, 2, args));
}

int32_t calculator_client_mul1(connector_object_handle obj, int32_t a, int32_t b)
{
    connector_value args[2] = { connector_value_createInt32__(a), connector_value_createInt32__(b) };
    return connector_value_getInt32(connector_object_callMethod(obj, CALCULATOR_MUL1_ID, 2, args));
}

int32_t calculator_client_div1(connector_object_handle obj, int32_t a, int32_t b)
{
    connector_value args[2] = { connector_value_createInt32__(a), connector_value_createInt32__(b) };
    return connector_value_getInt32(connector_object_callMethod(obj, CALCULATOR_DIV1_ID, 2, args));
}

int32_t calculator_client_add2(connector_object_handle obj, int32_t a, int32_t b, int32_t c)
{
    connector_value args[3] = { connector_value_createInt32__(a), connector_value_createInt32__(b), connector_value_createInt32__(c) };
    return connector_value_getInt32(connector_object_callMethod(obj, CALCULATOR_ADD2_ID, 3, args));
}
#include <calculator/loader.h>

#include <calculator/service.h>

#include <connector/root_context.h>

#include <stdio.h>

void calculator_load()
{
    printf("calculator_load\n");
    static connector_external_object_func_table calculator_service_table = {
        .destroy__ = &calculator_service_destroy__,
        .callMethod = &calculator_service_callMethod
    };

    connector_object_handle calculator_service_object = connector_external_object_create__(calculator_service_create__(), calculator_service_table);
    connector_root_context_registerObject("calculator", calculator_service_object);
    connector_object_unref__(calculator_service_object);
}

void calculator_unload()
{
    connector_root_context_removeObject("calculator");
}

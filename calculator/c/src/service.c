#include <calculator/service.h>

#include <connector/external_object.h>

#include <stdio.h>

// #define TRACE(...)
#define TRACE(...) printf(__VA_ARGS__)

#define CALCULATOR_ADD1_ID 0
#define CALCULATOR_SUB1_ID 1
#define CALCULATOR_MUL1_ID 2
#define CALCULATOR_DIV1_ID 3
#define CALCULATOR_ADD2_ID 4

int32_t calculator_service_add1(int32_t a, int32_t b)
{
    TRACE("[trace][calculator][int32_t calculator_service_add1(int32_t, int32_t)]\n");

    return a + b;
}

int32_t calculator_service_sub1(int32_t a, int32_t b)
{
    TRACE("[trace][calculator][int32_t calculator_service_sub1(int32_t, int32_t)]\n");

    return a - b;
}

int32_t calculator_service_mul1(int32_t a, int32_t b)
{
    TRACE("[trace][calculator][int32_t calculator_service_mul1(int32_t, int32_t)]\n");

    return a * b;
}

int32_t calculator_service_div1(int32_t a, int32_t b)
{
    TRACE("[trace][calculator][int32_t calculator_service_div1(int32_t, int32_t)]\n");

    return a / b;
}

int32_t calculator_service_add2(int32_t a, int32_t b, int32_t c)
{
    TRACE("[trace][calculator][int32_t calculator_service_add2(int32_t, int32_t, int32_t)]\n");

    return a + b + c;
}

connector_external_object_handle calculator_service_create__()
{
    TRACE("[trace][calculator][connector_external_object_handle calculator_service_create__()]\n");

    return (connector_external_object_handle)(NULL);
}

void calculator_service_destroy__(connector_external_object_handle)
{
    TRACE("[trace][calculator][void calculator_service_destroy__(connector_external_object_handle)]\n");
}

connector_value calculator_service_callMethod(connector_external_object_handle, uint32_t id, size_t argc, const connector_value* argv)
{
    TRACE("[trace][calculator][int32_t calculator_service_callMethod(connector_external_object_handle, uint32_t, size_t, const int32_t*)]\n");

    switch (id) {
    case CALCULATOR_ADD1_ID:
        return connector_value_createInt32__(calculator_service_add1(connector_value_getInt32(argv[0]), connector_value_getInt32(argv[1])));
    case CALCULATOR_SUB1_ID:
        return connector_value_createInt32__(calculator_service_sub1(connector_value_getInt32(argv[0]), connector_value_getInt32(argv[1])));
    case CALCULATOR_MUL1_ID:
        return connector_value_createInt32__(calculator_service_mul1(connector_value_getInt32(argv[0]), connector_value_getInt32(argv[1])));
    case CALCULATOR_DIV1_ID:
        return connector_value_createInt32__(calculator_service_div1(connector_value_getInt32(argv[0]), connector_value_getInt32(argv[1])));
    case CALCULATOR_ADD2_ID:
        return connector_value_createInt32__(calculator_service_add2(connector_value_getInt32(argv[0]), connector_value_getInt32(argv[1]), connector_value_getInt32(argv[2])));
    default:
        return connector_value_createInt32__(0);
    }
    return connector_value_createInt32__(0);
}

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void calculator_load();
void calculator_unload();

#ifdef __cplusplus
}
#endif

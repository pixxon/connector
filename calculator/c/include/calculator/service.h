#pragma once

#include <connector/external_object.h>

connector_external_object_handle calculator_service_create__();
void calculator_service_destroy__(connector_external_object_handle);
connector_value calculator_service_callMethod(connector_external_object_handle, uint32_t id, size_t argc, const connector_value* argv);

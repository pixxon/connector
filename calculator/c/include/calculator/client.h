#pragma once

#include <connector/object.h>

int32_t calculator_client_add1(connector_object_handle obj, int32_t a, int32_t b);

int32_t calculator_client_sub1(connector_object_handle obj, int32_t a, int32_t b);

int32_t calculator_client_mul1(connector_object_handle obj, int32_t a, int32_t b);

int32_t calculator_client_div1(connector_object_handle obj, int32_t a, int32_t b);

int32_t calculator_client_add2(connector_object_handle obj, int32_t a, int32_t b, int32_t c);

from calculator import Loader, Tests

class CalculatorTests(Tests):
	@classmethod
	def setUpClass(cls):
		Loader.load()

	@classmethod
	def tearDownClass(cls):
		Loader.unload()

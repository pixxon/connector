from ctypes import *

from calculator import Tests

class CalculatorTests(Tests):
    calculator = CDLL("libcalculator-0.0.1.so")
    
    @classmethod
    def setUpClass(cls):
        CalculatorTests.calculator.calculator_load()

    @classmethod
    def tearDownClass(cls):
        CalculatorTests.calculator.calculator_unload()

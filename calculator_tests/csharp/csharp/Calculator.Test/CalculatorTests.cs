namespace Calculator.Test;

using Calculator;

public class CalculatorTests : Tests, IDisposable
{
    public CalculatorTests()
    {
        Loader.Load();
    }

    public void Dispose()
    {
        Loader.Unload();
    }
}
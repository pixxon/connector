cmake_minimum_required(VERSION 3.22)

project(calculator
    LANGUAGES CXX
    VERSION 0.0.1
)

find_package(calculatorcxx CONFIG REQUIRED)

set(target_name "tests_cpp_cpp")

enable_testing()

find_package(GTest REQUIRED)

add_executable(${target_name}_test
    src/calculator.cpp
)

target_link_libraries(${target_name}_test
    calculatorcxx::calculatorcxx
    GTest::gtest
    GTest::gtest_main
)

include(GoogleTest)
gtest_discover_tests(${target_name}_test)

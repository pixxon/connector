#include <calculator/client.hpp>
#include <calculator/loader.hpp>
#include <calculator/tests.hpp>

#include <gtest/gtest.h>

struct calculator_test
    : ::testing::Test {
protected:
    void SetUp() override
    {
        calculator::loader::load();
    }

    void TearDown() override
    {
        calculator::loader::unload();
    }
};

TEST_F(calculator_test, add1)
{
    test_calculator_add1();
}

TEST_F(calculator_test, sub1)
{
    test_calculator_sub1();
}

TEST_F(calculator_test, mul1)
{
    test_calculator_mul1();
}

TEST_F(calculator_test, div1)
{
    test_calculator_div1();
}

TEST_F(calculator_test, add2)
{
    test_calculator_add2();
}

#include <calculator/loader.h>
#include <calculator/tests.h>

#include <check.h>
#include <stdlib.h>

void test_calculator_setup(void)
{
    calculator_load();
}

void test_calculator_teardown(void)
{
    calculator_unload();
}

int main(void)
{
    SRunner* sr = srunner_create(NULL);
    srunner_add_suite(sr, test_calculator_suite(&test_calculator_setup, &test_calculator_teardown));

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

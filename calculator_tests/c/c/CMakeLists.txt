cmake_minimum_required(VERSION 3.22)

project(calculator
    LANGUAGES C CXX
    VERSION 0.0.1
)

find_package(calculator CONFIG REQUIRED)

set(target_name "calculator_tests_c_c")

enable_testing()

find_package(check REQUIRED)

add_executable(${target_name}
    src/main.c
)
target_link_libraries(${target_name}
    Check::check
    calculator::calculator
)

macro(add_check_test suite case)
    add_test(
        NAME ${suite}.${case}
        COMMAND $<TARGET_FILE:${target_name}>
    )
    set_tests_properties(
        ${suite}.${case}
    PROPERTIES
        ENVIRONMENT "CK_VERBOSITY=verbose;CK_RUN_SUITE=${ARGV1};CK_RUN_CASE=${ARGV2}"
    )
endmacro()

add_check_test("calculator" "test_add1")
add_check_test("calculator" "test_sub1")
add_check_test("calculator" "test_mul1")
add_check_test("calculator" "test_div1")
add_check_test("calculator" "test_add2")

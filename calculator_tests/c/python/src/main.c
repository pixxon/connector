#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <calculator/tests.h>

#include <check.h>
#include <stdlib.h>

PyObject* module = NULL;
PyObject* loader = NULL;

void test_calculator_setup(void)
{
    Py_Initialize();
    module = PyImport_ImportModule("calculator");
    loader = PyObject_GetAttrString(module, "Loader");
    PyObject* load = PyUnicode_FromString("load");
    PyObject_CallMethodNoArgs(loader, load);
    Py_DECREF(load);
}

void test_calculator_teardown(void)
{
    PyObject* unload = PyUnicode_FromString("unload");
    PyObject_CallMethodNoArgs(loader, unload);
    Py_DECREF(unload);
    Py_DECREF(loader);
    Py_DECREF(module);
    Py_Finalize();
}

int main(void)
{
    SRunner* sr = srunner_create(NULL);
    srunner_add_suite(sr, test_calculator_suite(&test_calculator_setup, &test_calculator_teardown));

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

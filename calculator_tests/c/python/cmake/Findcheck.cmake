include(FindPackageHandleStandardArgs)

find_library(CHECK_LIBRARY NAMES check)
find_path(CHECK_INCLUDE_DIR NAMES check.h)

find_package_handle_standard_args(check REQUIRED_VARS CHECK_LIBRARY CHECK_INCLUDE_DIR)

if (CHECK_FOUND)
  mark_as_advanced(CHECK_INCLUDE_DIR)
  mark_as_advanced(CHECK_LIBRARY)
endif()

add_library(check::check IMPORTED UNKNOWN)
set_property(TARGET check::check PROPERTY IMPORTED_LOCATION ${CHECK_LIBRARY})
target_include_directories(check::check INTERFACE ${CHECK_INCLUDE_DIR})

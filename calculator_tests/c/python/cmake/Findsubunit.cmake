include(FindPackageHandleStandardArgs)

find_library(SUBUNIT_LIBRARY NAMES subunit)
# find_path(SUBUNIT_INCLUDE_DIR NAMES subunit.h)

find_package_handle_standard_args(subunit REQUIRED_VARS SUBUNIT_LIBRARY) # SUBUNIT_INCLUDE_DIR

if (SUBUNIT_FOUND)
#  mark_as_advanced(SUBUNIT_INCLUDE_DIR)
  mark_as_advanced(SUBUNIT_LIBRARY)
endif()

add_library(subunit::subunit IMPORTED UNKNOWN)
set_property(TARGET subunit::subunit PROPERTY IMPORTED_LOCATION ${SUBUNIT_LIBRARY})
# target_include_directories(subunit::subunit INTERFACE ${SUBUNIT_INCLUDE_DIR})
